import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthService } from '../authentication/AuthService';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;
  public showFormErr = false;

  private credentials = {
    username: '',
    password: ''
  };

  constructor(private formBuilder: FormBuilder, private auth: AuthService, private router: Router) {
    this.createLoginForm();
  }

  ngOnInit() {
  }

  public showError(formValue: string): boolean {
    return !this.loginForm.get(formValue).valid && this.showFormErr;
  }

  public onLoginSubmit(model) {
    if (this.loginForm.invalid) {
      this.showFormErr = true;
    } else {

      this.credentials = model;
      this.auth.authenticate(this.credentials, () => {
        this.router.navigateByUrl('/');
      });

      this.loginForm.reset();
      return false;

    }
  }

  private createLoginForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.email],
      password: ['', Validators.required]
    });
  }



}
