import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RaceListComponent } from './content/race/race-list/race-list.component';
import { ContentComponent } from './content/content.component';
import {UpcomingComponent} from './content/race/upcoming/upcoming.component';
import {RunfurtherComponent} from './content/runfurther/runfurther.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {
    path: '',
    redirectTo: '/races/upcoming',
    pathMatch: 'full'
  },
  {
    path: 'races',
    component: ContentComponent,
    children: [
      {
        path : 'upcoming',
        component: UpcomingComponent
      },
      {
        path : 'all-races',
        component: RaceListComponent
      },
      {
        path : 'runfurther',
        component: RunfurtherComponent
      },


    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
