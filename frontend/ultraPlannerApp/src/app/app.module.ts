import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule} from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RaceModule} from './content/race/race.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgMaterialModule} from './ng-material/ng-material.module';
import { ErrorModalComponent } from './error-modal/error-modal.component';
import { LoginComponent } from './login/login.component';
import { RunfurtherComponent } from './content/runfurther/runfurther.component';
import { RaceInfoComponent } from './content/runfurther/race-info/race-info.component';
import { FooterComponent } from './content/footer/footer.component';
import {AuthService} from './authentication/AuthService';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpInterceptors} from './authentication/HttpInterceptor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ContactFormComponent } from './content/footer/contact-form/contact-form.component';
import {ContentComponent} from "./content/content.component";

@NgModule({
  declarations: [
    AppComponent,
    ErrorModalComponent,
    LoginComponent,
    RunfurtherComponent,
    RaceInfoComponent,
    FooterComponent,
    ContactFormComponent,
    ContentComponent
  ],
  imports: [
    BrowserModule,
    NgMaterialModule,
    AppRoutingModule,
    NgbModule,
    RaceModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule
    
  ],
  providers: [AuthService, { provide: HTTP_INTERCEPTORS, useClass: HttpInterceptors, multi: true }],
  bootstrap: [AppComponent],
  exports: [],
  entryComponents: [
    ErrorModalComponent
  ]
})
export class AppModule { }
