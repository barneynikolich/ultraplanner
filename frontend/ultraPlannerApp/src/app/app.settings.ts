export class AppSettings {
  // TODO: Place all urls in here
  public static baseUrl: string = 'api/v1';

  // Example url
  public static reportsBaseUrl: string = `${AppSettings.baseUrl}report/`;

}
