import { NgModule } from '@angular/core';
import {
  MatButtonModule, MatCardModule, MatCheckboxModule, MatChipsModule, MatDialogModule, MatExpansionModule,
  MatFormFieldModule,
  MatInputModule, MatProgressBarModule, MatSnackBarModule
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
  imports: [
    MatButtonModule,
    MatFormFieldModule,
    MatDialogModule,
    MatInputModule,
    MatCardModule,
    MatSnackBarModule,
    BrowserAnimationsModule,
    MatChipsModule,
    MatProgressBarModule,
    MatExpansionModule
  ],
  exports: [MatButtonModule, MatFormFieldModule, MatDialogModule, MatInputModule, MatCardModule,
    MatSnackBarModule, BrowserAnimationsModule, MatChipsModule, MatProgressBarModule, MatExpansionModule],

  declarations: []
})
export class NgMaterialModule { }
