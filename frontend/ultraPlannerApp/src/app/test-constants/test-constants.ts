import {Race} from "../content/race/models/race";

export function TERRAINS(): string[] {
  return ['Coastal', 'Fell', 'Farmland', 'Trails'];
}

export function LOCATIONS(): string[] {
  return ['Manchester', 'Peak District', 'Yorkshire Dales', 'North York Moors'];
}

export function RACES(): Race[] {
  return [
    new Race(1, 'Three Peaks', '12/10/1994', 'Yorkshire Dales', 30, null, 'Fell', null, null, null, '24', null, null, null),
    new Race(2, 'Hardmoor', '12/10/1994', 'somewhere', 66, null, 'Fell', null, null, null, '24', null, null, null),
    new Race(3, 'Hills', '12/10/1994', 'Manchester', 28, null, 'Coastal', null, null, null, '24', null, null, null),
    new Race(4, 'Somerace_4', '12/10/1994', 'Peak District', 45, null, 'Fell', null, null, null, '24', null, null, null),
    new Race(5, 'somerace_5', '12/10/1994', 'Yorkshire Dales', 141, null, 'Fell', null, null, null, '24', null, null, null),
    new Race(6, 'Somerace_6', '12/10/1994', 'Peak District', 41, null, 'Fell', null, null, null, '24', null, null, null),
    new Race(7, 'Somerace_7', '12/10/1994', 'Peak District', 77, null, 'Coastal', null, null, null, '24', null, null, null),
    new Race(8, 'Somerace_8', '12/10/1994', 'Peak District', 65, null, 'Fell', null, null, null, '24', null, null, null)
  ];
}
