import {Injectable} from '@angular/core';
import {HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';

@Injectable()
export class HttpInterceptors implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler) {

    // console.log('Intercepted http request!');

    const xhr = req.clone({
      // headers: req.headers.set('X-Requested-With', 'XMLHttpRequest')
    });
    return next.handle(xhr);
  }
}
