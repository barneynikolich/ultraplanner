import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {environment} from '../../environments/environment';

@Injectable()
export class AuthService {

  authenticated = false;
  private baseUrl = environment.localhost;

  constructor(private http: HttpClient) {
  }

  public authenticate(credentials, callback): void {
    console.log(credentials);

    const headers = new HttpHeaders(credentials ? {
      authorization : 'Basic ' + btoa(credentials.username + ':' + credentials.password)
    } : {});

    this.http.get(this.baseUrl + 'login', {headers: headers}).subscribe(response => {
      console.log(response);
      if (response['name']) {
        this.authenticated = true;
      } else {
        this.authenticated = false;
      }
      return callback && callback();
    });

  }

}
