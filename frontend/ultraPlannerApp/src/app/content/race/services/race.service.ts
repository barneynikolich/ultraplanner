import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Race} from '../models/race';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import {environment} from '../../../../environments/environment';
import {Area} from '../../../models/distance';
import {Filters} from '../../../models/filters';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

@Injectable()
export class RaceService {

  private _getRacesFromServer: BehaviorSubject<Race[]> = new BehaviorSubject([]);
  private _getRacesFromServer$: Observable<Race[]> = this._getRacesFromServer.asObservable();

  private baseUrl = environment.localhost;
  public races: Race[];
  public filteredRaces: Race[];
  public upcomingRaces: Race[];

  constructor(private httpClient: HttpClient) {
  }

  public getRacesFromServer(filters: Filters): void {
    this.findAll(filters).subscribe((races: Race[]) => {
      this._getRacesFromServer.next(races);
    });
  }

  get retrieveRacesFromServer(): Observable<Race[]> {
    return this._getRacesFromServer$;
  }

  public findAll(filter?: Filters): Observable<Race[]> {
    const url = this.baseUrl + 'races';
    let params = new HttpParams();

    if (filter) {
      for (const key in filter.filters) {
        params = params.append(key, filter.filters[key]);
      }
    }

    return this.httpClient.get<Race[]>(url, {params: params}).map((races: Race[]) => {
      if (filter) {
        this.filteredRaces = races;
      } else {
        this.races = races;
      }
      return races;
    })
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  public getUpcomingRaces(): Observable<any> {
    const url = this.baseUrl + 'races/upcoming';
    return this.httpClient.get(url).map((races: Race[]) => {
      this.upcomingRaces = races;
      return races;
    });
  }

  public deleteRace(race: Race): Observable<any> {
    const url = this.baseUrl + 'races/' + race.id;
    return this.httpClient.delete(url);
  }

  public saveRace(race: Race): Observable<Object> {
    const url = this.baseUrl + 'races';
    return this.httpClient.post(url, race).catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  public editRace(race: Race): Observable<Object> {
    const url = this.baseUrl + 'races';
    return this.httpClient.put(url, race).catch((error: any) => Observable.throw(error.json().error || 'Server error'));

  }

  public getLocations(): Observable<Object> {
    return this.httpClient.get(this.baseUrl + 'locations');
  }

  public getRaceLocations(): Area[] {
    const raceAreaCount = new Map<string, number>();
    const locations = [];
    const temp = [];


    const races = this.getRacesFromCache();
    if (races) {
      races.forEach((race: Race) => {
        if ((locations.indexOf(race.area) === -1) && (race.area !== '') && (race.area !== null)) {
          locations.push(race.area);
          raceAreaCount.set(race.area, 1);
        } else {
          raceAreaCount.set(race.area, raceAreaCount.get(race.area) + 1);
        }
      });
    }

    raceAreaCount.forEach((k, v) => {
      locations.map((area: string) => {
        if (v === area) {
          temp.push(new Area(area, k));
        }
      });
    });
    return temp.sort((left: Area, right: Area) => {
      if (left.area < right.area) { return -1; }
      if (left.area > right.area) { return 1; }
      return 0;
    });
  }

  public getTerrains(): Observable<Object> {
    return this.httpClient.get(this.baseUrl + 'terrains');
  }

  public getRacesFromCache(): Race[] {
    return this.races;
  }



}

