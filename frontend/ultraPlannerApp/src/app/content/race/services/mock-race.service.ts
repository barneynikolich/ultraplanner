import {Observable} from 'rxjs/Observable';
import {Race} from '../models/race';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

export class MockRaceService {

  findAll(): Observable<Race[]> {
    return Observable.of([]);
  }

  public deleteRace(race: Race) {

  }

  public getLocations() {
  }

  public getTerrains() {
  }

  public getRacesFromCache(): Race[] {
    return [];
  }

  public getUpcomingRaces(): Observable<Race[]> {
    return Observable.of([]);
  }




}

