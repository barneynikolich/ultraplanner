import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { RaceRoutingModule } from './race-routing.module';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {RaceListComponent} from './race-list/race-list.component';
import {RaceDetailComponent} from './race-list/race-detail/race-detail.component';
import {RaceListTableComponent} from './race-list/race-list-table/race-list-table.component';
import {RaceService} from './services/race.service';
import {NgMaterialModule} from '../../ng-material/ng-material.module';
import {NgbModalStack} from '@ng-bootstrap/ng-bootstrap/modal/modal-stack';
import {AddRaceModalComponent} from './add-race-modal-component/add-race-modal-component';
import { RaceFilterComponent } from './race-list/race-filter/race-filter.component';
import { HttpClientModule } from '@angular/common/http';
import { UpcomingComponent } from './upcoming/upcoming.component';
import { MonthAccordianComponent } from './race-list/month-accordian/month-accordian.component';
import {RaceFilterService} from './race-list/services/race-filter-service/race-filter.service';
import {NotificationService} from './race-list/services/notification-service/notification.service';
import {RaceMetaDataService} from './race-list/services/race-meta-data-service/race-meta-data.service';
import {ModalService} from './race-list/services/modal-service/modal.service';
import {RaceAccordianService} from './race-list/services/accordian-service/race-accordian.service';
import { TypeaheadSearchComponent } from './race-list/typeahead-search/typeahead-search.component';
import { ProgressBarService } from '../services/progress-bar.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    CommonModule,
    RaceRoutingModule,
    NgbModule,
    FormsModule,
    NgMaterialModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule
  ],
  declarations: [
    RaceListComponent, RaceDetailComponent, RaceListTableComponent, AddRaceModalComponent, RaceFilterComponent, RaceFilterComponent,
    UpcomingComponent,
    MonthAccordianComponent,
    TypeaheadSearchComponent
  ],
  exports: [RaceListComponent, RaceDetailComponent, RaceListTableComponent, TypeaheadSearchComponent],
  providers: [
    RaceService,
    NgbModalStack,
    RaceFilterComponent,
    RaceFilterService,
    NotificationService,
    RaceMetaDataService,
    ModalService,
    RaceAccordianService,
    ProgressBarService
  ],
  entryComponents: [
    AddRaceModalComponent
  ],

})
export class RaceModule { }
