import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaceListTableComponent } from './race-list-table.component';
import { RaceService } from '../../services/race.service';
import { NgbAlertConfig, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RaceDetailComponent } from '../race-detail/race-detail.component';
import {HttpModule} from '@angular/http';
import {HttpClientModule} from '@angular/common/http';

describe('RaceListTableComponent', () => {
  let component: RaceListTableComponent;
  let fixture: ComponentFixture<RaceListTableComponent>;
  let raceService: RaceService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgbModule, HttpClientModule, HttpModule],
      declarations: [ RaceListTableComponent, RaceDetailComponent ],
      providers: [RaceService, NgbAlertConfig]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaceListTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    raceService = fixture.debugElement.injector.get(RaceService);
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
