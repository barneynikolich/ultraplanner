import {Component, Input} from '@angular/core';
import {Race} from '../../models/race';
import {MonthRace} from '../../../../models/monthRace';

@Component({
  selector: 'app-race-list-table',
  template: '<p>Mock Race List Table</p>'
})
export class MockRaceListTableComponent {

  @Input()
  public races: Race[];
  @Input()
  public showFilters = false;
  @Input()
  public racesMonths: MonthRace;
}
