import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Race} from '../../models/race';
import {RaceService} from '../../services/race.service';
import {MonthRace} from '../../../../models/monthRace';
import {animate, style, transition, trigger} from "@angular/animations";

@Component({
  selector: 'app-race-list-table',
  templateUrl: './race-list-table.component.html',
  styleUrls: ['./race-list-table.component.css'],
  animations: [
    trigger('EnterLeave', [
      // state('flyIn', style({ transform: 'translateX(0)' })),
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('0.3s 0.2s ease-in')
      ]),
      transition(':leave', [
        animate('0.3s ease-out', style({ transform: 'translateX(100%)' }))
      ])
    ])
  ]

})
export class RaceListTableComponent implements OnInit {

  public months = ['January', 'February', 'March', 'April', 'May', 'June', 'July',
    'August', 'September', 'October', 'November', 'December'];

  @Input()
  public races: Race[];

  @Input()
  public racesMonths: MonthRace;

  @Input()
  public showFilters = false;

  @Output()
  public onRaceDeleted = new EventEmitter<Race>();

  @Output()
  public onRaceEdited = new EventEmitter<Race>();

  @Output()
  public onFilter: EventEmitter<Map<string, string>> = new EventEmitter();

  public selectedRows: string[] = [];
  public locations = this.raceService.getRaceLocations();

  constructor(private raceService: RaceService) {
  }

  ngOnInit() {
  }

  public onDeleteRace(event: Race): void {
    this.onRaceDeleted.emit(event);
  }

  public onEditRace(race: Race): void {
    this.onRaceEdited.emit(race);
  }

  public handleTypeaheadSubmit(event) {
    const searchRace: Race = event.item;
  }

  private toggleDetail(row: string) {
    if (this.rowExists(row)) {
      this.removeRow(row);
    } else {
      this.addRow(row);
    }
  }

  private showRaceDetail(row): boolean {
    if (this.rowExists(row)) {
      return true;
    } else {
      return false;
    }
  }

  private removeRow(row: string) {
    const index = this.selectedRows.indexOf(row);
    if (index !== -1) {
      this.selectedRows.splice(index, 1);
    }
  }

  private addRow(row: string) {
    this.selectedRows.push(row);
  }

  private rowExists(row: string): boolean {
    let exists = false;
    this.selectedRows.forEach( elem => {
      if (elem === row) {
        exists = true;
      }
    });
    return exists;
  }

  private isDateRow(row: Race): boolean {
    return  this.isDate(row);
  }

  private isDate(row: Race): boolean {
    if (row.name.match('(January 201)|(February 2017)|(March 2017)|(April.*)|(May.*)|(June.*)' +
        '|(July.*)|(August.*)|(September.*)|(October.*)|(November.*)|(December.*)')) {
      return true;
    } else {
      return false;
    }
  }

}
