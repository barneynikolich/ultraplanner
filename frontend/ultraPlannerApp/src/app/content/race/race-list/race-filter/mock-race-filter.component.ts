import {Component, Input} from '@angular/core';
import {Area} from '../../../../models/distance';

@Component({
  selector: 'app-race-filter',
  template: '<p>Mock app race filter</p>'
})
export class MockRaceFilterComponent {
  @Input()
  public resultCount: number;
  @Input()
  public locations: Area[];
  @Input()
  public terrains: string[];
}
