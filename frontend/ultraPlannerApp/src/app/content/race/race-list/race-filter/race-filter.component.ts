import { Component, OnInit, EventEmitter, Input, Output, state, style, animate, trigger, transition, OnChanges } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TableFilterLogic} from '../../../../models/table-filter-logic';
import {Location} from '../model/location';
import { Filters } from '../../../../models/filters';
import {DistanceOptions} from '../../../../models/distanceOptions';

@Component({
  selector: 'app-race-filter',
  templateUrl: './race-filter.component.html',
  styleUrls: ['./race-filter.component.css'],
})
export class RaceFilterComponent implements OnInit {

  @Input()
  public locations: Location[];

  @Input()
  public terrains: string[];

  @Input()
  public resultCount: number;

  public distanceOptions = [DistanceOptions.Twenty_Six_to_49, DistanceOptions.Fifty_to_69,
    DistanceOptions.Seventy_to_99, DistanceOptions.Greater_100];

  public filterRacesForm: FormGroup;

  @Output()
  public onFilterRaces = new EventEmitter<Filters>();

  @Output()
  public onResetRacesAndCount = new EventEmitter<boolean>();

  private _filters: Filters = new Filters();

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.createFilterRaceForm();
  }

  public filterRacesSubmit(model: TableFilterLogic) {
    if (this.formIsNotEmpty()) {
      this.setFiltersFromModel(model, false);
      this.onFilterRaces.emit(this._filters);
    } else {
      this.onResetRacesAndCount.emit(true);
    }
  }

  private formIsNotEmpty(): boolean {
    return this.filterRacesForm.get('distance').value !== '' || this.filterRacesForm.get('area').value !== ''
      || this.filterRacesForm.get('terrain').value !== '' || this.filterRacesForm.get('dateSort').value !== '';
  }

  public getResultCount(model: TableFilterLogic) {
    if (this.formIsNotEmpty()) {
      this.setFiltersFromModel(model, true);
      this.onFilterRaces.emit(this._filters);
    } else {
      this.onResetRacesAndCount.emit();
    }
  }

  public resetFilterForm(): void {
    this.filterRacesForm.setValue({
      distance: '',
      area: '',
      terrain: '',
      dateSort: ''
    });
  }

  private setFiltersFromModel(model: TableFilterLogic, raceCount: boolean): void {
    this._filters.reset();
    this._filters.addFilter('distance', model.distance);
    this._filters.addFilter('area', model.area);
    this._filters.addFilter('terrain', model.terrain);
    this._filters.addFilter('dateSort', model.dateSort);
    this._filters.getRaceCount = raceCount;
  }

  private createFilterRaceForm() {
    this.filterRacesForm = this.formBuilder.group({
      distance:  ['', Validators.required ],
      area: ['', Validators.required ],
      terrain: [''],
      dateSort: ['']
    });
  }
}
