import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaceFilterComponent } from './race-filter.component';
import { ReactiveFormsModule } from '@angular/forms';
import {TableFilterLogic} from '../../../../models/table-filter-logic';

describe('RaceFilterComponent', () => {
  let componentUnderTest: RaceFilterComponent;
  let fixture: ComponentFixture<RaceFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [ RaceFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RaceFilterComponent);
    componentUnderTest = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(componentUnderTest).toBeTruthy();
  });

  it('should emit onFilterRaces with filterLogic object when form is submitted with all values not empty', () => {
    spyOn(componentUnderTest.onFilterRaces, 'emit');

    const filterLogic = new TableFilterLogic('26 - 49', 'Yorkshire Dales', 'Coastal', false, 'asc');
    componentUnderTest.filterRacesForm.setValue({
      distance: '26 - 49',
      area: 'Yorkshire Dales',
      terrain: 'Coastal',
      dateSort: 'asc'
    });

    componentUnderTest.filterRacesSubmit(filterLogic);

    expect(componentUnderTest.onFilterRaces.emit).toHaveBeenCalled();
  });

  it('should not emit onFilterRaces when form is submitted empty', () => {
    spyOn(componentUnderTest.onFilterRaces, 'emit');

    componentUnderTest.filterRacesSubmit(undefined);
    expect(componentUnderTest.onFilterRaces.emit).not.toHaveBeenCalled();
  });

});
