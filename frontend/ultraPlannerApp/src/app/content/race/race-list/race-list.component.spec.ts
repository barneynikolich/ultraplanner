import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RaceListComponent} from './race-list.component';
import {Page} from '../../../page.test';
import {Race} from '../models/race';
import {RaceService} from '../services/race.service';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import {DebugElement, NO_ERRORS_SCHEMA} from '@angular/core';
import {MockRaceService} from '../services/mock-race.service';
import {NgbAlert} from '@ng-bootstrap/ng-bootstrap';
import {NotificationService} from './services/notification-service/notification.service';
import {MockNotificationService} from './services/notification-service/mock-notification.service';
import {RaceMetaDataService} from './services/race-meta-data-service/race-meta-data.service';
import {ModalService} from './services/modal-service/modal.service';
import {MockModalService} from './services/modal-service/mock-modal.service';
import {RaceAccordianService} from './services/accordian-service/race-accordian.service';
import {ProgressBarService} from '../../services/progress-bar.service';
import {MockProgressBarService} from '../../services/mock-progress-bar.service';
import {MockRaceMetaDataService} from './services/race-meta-data-service/mock-race-meta-data.service';
import {MockTypeAheadSearchComponent} from './typeahead-search/mock-typeahead-search.component';
import {MockRaceListTableComponent} from './race-list-table/mock-race-list-table.component';
import {MockRaceFilterComponent} from './race-filter/mock-race-filter.component';
import {MockMonthAccordianComponent} from './month-accordian/mock-month-race-accordian.component';
import {ProgressEvent} from '../../services/progress-bar-event.enum';
import {MonthRace} from '../../../models/monthRace';
import {Subject} from "rxjs";


describe('RaceListComponent', () => {

  let componentUnderTest: RaceListComponent;
  let fixtureUnderTest: ComponentFixture<RaceListComponent>;
  let page: RaceListPage;
  let debugElement: DebugElement;

  let mockRaceService: MockRaceService;
  let mockProgressBarService: ProgressBarService;
  let mockRaceMetaDataService: RaceMetaDataService;

  let mockUpdateRacesSubject: Subject<boolean>;
  let mockRaceDataModelSubject: Subject<any[]>;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      declarations: [
        RaceListComponent,
        NgbAlert
      ],
      providers: [
        {provide: RaceService, useClass: MockRaceService},
        {provide: NotificationService, useClass: MockNotificationService},
        {provide: RaceMetaDataService, useClass: MockRaceMetaDataService},
        {provide: ModalService, useClass: MockModalService},
        {provide: RaceAccordianService},
        {provide: ProgressBarService, useClass: MockProgressBarService}
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();

    fixtureUnderTest = TestBed.createComponent(RaceListComponent);
    componentUnderTest = fixtureUnderTest.componentInstance;
    page = new RaceListPage(fixtureUnderTest.debugElement);
    debugElement = fixtureUnderTest.debugElement;

    mockRaceService = TestBed.get(RaceService);
    mockProgressBarService = TestBed.get(ProgressBarService);
    mockRaceMetaDataService = TestBed.get(RaceMetaDataService);
    mockUpdateRacesSubject = new Subject<false>();
    mockRaceDataModelSubject = new Subject<any[]>()

  }));

  describe('Calling ngOnInit', () => {
    it ('shouldUpdateRaces if racesUpdated is true and races are cached', () => {
      setUpRaceListComponent(true, races(), locations(), terrains(), true);
      fixtureUnderTest.detectChanges();

      expect(mockProgressBarService.showProgressBar).toHaveBeenCalledWith(ProgressEvent.LOADING_ALL_RACES);
      expect(componentUnderTest.races.length).toBe(races().length);

    });

    it ('should set races from cache', () => {
      setUpRaceListComponent(false, races(), locations(), terrains(), true);
      fixtureUnderTest.detectChanges();

      expect(componentUnderTest.races.length).toBe(races().length);
      expect(mockRaceMetaDataService.populateMonthRaces).toHaveBeenCalled();
    });

    function setUpRaceListComponent(racesUpdated: boolean, races: Race[], locations: string[], terrains: string[], retrieveFromCache: boolean) {
      spyOn(mockRaceMetaDataService, 'populateMonthRaces').and.returnValue(new MonthRace);
      spyOn(mockProgressBarService, 'showProgressBar');
      if (retrieveFromCache) {
        spyOnProperty(mockRaceMetaDataService, 'races', 'get').and.returnValue(races);
      } else {
        spyOnProperty(mockRaceMetaDataService, 'races', 'get').and.returnValue(undefined);
      }
      spyOnProperty(mockRaceMetaDataService, 'racesUpdated$', 'get').and.returnValue(Observable.of(racesUpdated));
      spyOn(mockRaceMetaDataService, 'initialiseRaceMetadata').and.returnValue(Observable.of( [locations, terrains, races]));
    }

    // it ('should call RaceService to get races when not in cache', () => {
    //   initialiseComponentWithRacesLocationsAndTerrains();
    //   expect(mockProgressBarService.showProgressBar).toHaveBeenCalledWith(ProgressEvent.LOADING_ALL_RACES);
    //   expect(mockRaceService.getRacesFromCache).toHaveBeenCalled();
    //   expect(mockRaceService.findAll).toHaveBeenCalled();
    //   expect(componentUnderTest.races.length).toBe(8);
    // });
    //
    //
    // it ('should call ProgressBarService to show the ProgressBar', () => {
    //   initialiseComponentWithRacesLocationsAndTerrains();
    //     expect(mockProgressBarService.showProgressBar).toHaveBeenCalledWith(ProgressEvent.LOADING_ALL_RACES);
    // });
    //
    // it ('should get and set Race Locations by calling RaceService', () => {
    //   initialiseComponentWithRacesLocationsAndTerrains();
    //   expect(mockRaceService.getLocations).toHaveBeenCalled();
    //   expect(componentUnderTest.locations.length).toEqual(4);
    //
    // });
    //
    // it ('should get and set Race Terrains by calling RaceService', () => {
    //   initialiseComponentWithRacesLocationsAndTerrains();
    //   expect(mockRaceService.getTerrains).toHaveBeenCalled();
    //   expect(componentUnderTest.terrains.length).toEqual(4);
    // });


  });


  function update(): void {
    fixtureUnderTest.detectChanges();
    page.updatePage();
  }


  function terrains(): string[] {
    return ['Coastal', 'Fell', 'Farmland', 'Trails'];
  }

  function locations(): string[] {
    return ['Manchester', 'Peak District', 'Yorkshire Dales', 'North York Moors'];
  }

  function races(): Race[] {
    return [
      new Race(1, 'Three Peaks', '12/10/1994', 'Yorkshire Dales', 30, null, 'Fell', null, null, null, '24', null, null, null),
      new Race(2, 'Hardmoor', '12/10/1994', 'somewhere', 66, null, 'Fell', null, null, null, '24', null, null, null),
      new Race(3, 'Hills', '12/10/1994', 'Manchester', 28, null, 'Coastal', null, null, null, '24', null, null, null),
      new Race(4, 'Somerace_4', '12/10/1994', 'Peak District', 45, null, 'Fell', null, null, null, '24', null, null, null),
      new Race(5, 'somerace_5', '12/10/1994', 'Yorkshire Dales', 141, null, 'Fell', null, null, null, '24', null, null, null),
      new Race(6, 'Somerace_6', '12/10/1994', 'Peak District', 41, null, 'Fell', null, null, null, '24', null, null, null),
      new Race(7, 'Somerace_7', '12/10/1994', 'Peak District', 77, null, 'Coastal', null, null, null, '24', null, null, null),
      new Race(8, 'Somerace_8', '12/10/1994', 'Peak District', 65, null, 'Fell', null, null, null, '24', null, null, null)
    ];
  }

  class RaceListPage extends Page {

    private _componentDeleteAnchor: HTMLAnchorElement;
    private _componentEditAnchor: HTMLElement;

    public updatePage(): void {
      this._componentDeleteAnchor = this.getHtmlElement('.condition-delete') as HTMLAnchorElement;
      this._componentEditAnchor = this.getHtmlElement('.condition-edit') as HTMLElement;
    }

    get componentDeleteAnchor(): HTMLAnchorElement {
      return this._componentDeleteAnchor;
    }

    get componentEditAnchor(): HTMLElement {
      return this._componentEditAnchor;
    }
  }
});
