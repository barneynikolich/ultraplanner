import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RaceDetailComponent } from './race-detail.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {RaceService} from '../../services/race.service';
import {HttpModule} from '@angular/http';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {MockRaceService} from '../../services/mock-race.service';

describe('RaceDetailComponent', () => {
  let component: RaceDetailComponent;
  let fixture: ComponentFixture<RaceDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgbModule,
        HttpClientModule
      ],
      declarations: [ RaceDetailComponent ],
      providers: [
         {provide: RaceService, useClass: MockRaceService},
         ]
    })
    .compileComponents();
  }));

  beforeEach( () => {
    fixture = TestBed.createComponent(RaceDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
