import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Race} from '../../models/race';
import {RaceService} from '../../services/race.service';

@Component({
  selector: 'app-race-detail',
  templateUrl: './race-detail.component.html',
  styleUrls: ['./race-detail.component.css'],
  providers: []
})
export class RaceDetailComponent implements OnInit {

  public raceFromTypeAhead: Race;

  @Input()
  race: Race;

  @Output()
  public onDeleteRace = new EventEmitter<Race>();

  @Output()
  public onEditRace = new EventEmitter<Race>();

  constructor(private raceService: RaceService) {
  }

  ngOnInit() {
    // if (!this.raceFromTypeAhead) {
    //   this.sub = this.route.params.subscribe(params => {
    //     this.raceFromTypeAhead = this._raceService.findRace(params['name']);
    //   });
    // }
  }

  public deleteRace() {
    this.onDeleteRace.next(this.race);
  }

  public editRace() {
    this.onEditRace.emit(this.race);
  }


}
