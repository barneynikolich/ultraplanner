import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthAccordianComponent } from './month-accordian.component';
import {NgMaterialModule} from '../../../../ng-material/ng-material.module';
import {Race} from '../../models/race';

describe('MonthAccordianComponent', () => {
  let component: MonthAccordianComponent;
  let fixture: ComponentFixture<MonthAccordianComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgMaterialModule],
      declarations: [ MonthAccordianComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthAccordianComponent);
    component = fixture.componentInstance;
    const race1 = new Race(1, 'Three Peaks', '12/10/1994', 'Yorkshire Dales', 30, null, 'Fell', null, null, null, '24', null, null, null);
    const race2 = new Race(2, 'Hardmoor', '12/10/1994', 'somewhere', 66, null, 'Fell', null, null, null, '24', null, null, null);
    const race3 = new Race(3, 'Hills', '12/10/1994', 'Manchester', 28, null, 'Coastal', null, null, null, '24', null, null, null);
    component.races = [race1, race2, race3];

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
