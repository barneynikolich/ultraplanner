import {Component, Input} from '@angular/core';
import {Race} from '../../models/race';

@Component({
  selector: 'app-month-accordian',
  template: '<p>Mock app race accordian</p>'
})
export class MockMonthAccordianComponent {
  @Input()
  public name: string;
  @Input()
  public expandAll: boolean;
  @Input()
  public races: Race[];
}
