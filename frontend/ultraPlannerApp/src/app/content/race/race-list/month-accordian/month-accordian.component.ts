import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {Race} from '../../models/race';

@Component({
  selector: 'app-month-accordian',
  templateUrl: './month-accordian.component.html',
  styleUrls: ['./month-accordian.component.css']
})
export class MonthAccordianComponent implements OnInit {

  @Input()
  public name: string;

  @Input()
  public races: Race[];
  public sticky = false;
  public expanded = false;

  constructor(public el: ElementRef) {
  }

  ngOnInit() {
    this.expanded = false;
  }

  public expand(div: HTMLDivElement): string {
    if (this.expanded) {
      return div.scrollHeight + 'px';
    } else {
      return null;
    }
  }

  public monthClicked(btn: HTMLDivElement) {
    // const btn = event.target as HTMLButtonElement;
    const panel = btn.nextElementSibling as HTMLDivElement;
    btn.classList.toggle('active');

    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
      this.expanded = false;
    } else {
      this.expanded = true;
      panel.style.maxHeight = panel.scrollHeight + 'px';
    }
  }

  public scrollTop(): void {
    window.scroll(0, 0);
  }

  get offSetTop(): number {
    return this.el.nativeElement.offsetTop;
  }

  get offSetBottom(): number {
    return this.el.nativeElement.offsetHeight;
  }

}
