import {RaceService} from '../services/race.service';
import {Race} from '../models/race';
import {NgbAlertConfig, NgbTypeahead, NgbTypeaheadConfig} from '@ng-bootstrap/ng-bootstrap';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/map';
import {Filters} from '../../../models/filters';
import {MonthRace} from '../../../models/monthRace';
import {Location} from './model/location';
import {MonthAccordianComponent} from './month-accordian/month-accordian.component';
import {NotificationService} from './services/notification-service/notification.service';
import {RaceMetaDataService} from './services/race-meta-data-service/race-meta-data.service';
import {ModalService} from './services/modal-service/modal.service';
import {RaceAccordianService} from './services/accordian-service/race-accordian.service';
import {RaceDataModel} from "./services/race-meta-data-service/model/race-data.model";
import {Component, HostListener, OnInit, QueryList, ViewChildren} from "@angular/core";
import {fadeInFadeOut} from "../../../../../../../../ultra-planner/src/app/animations";

@Component({
  selector: 'app-race-list',
  templateUrl: './race-list.component.html',
  styleUrls: ['./race-list.component.css'],
  providers: [NgbAlertConfig, NgbTypeahead, NgbTypeaheadConfig],
  animations: [fadeInFadeOut]
})
export class RaceListComponent implements OnInit {

  public filteredRaces: Race[];

  public showErr = false;

  public showColumnFilters = false;

  public filteredRaceCount: number;

  public showFilteredRaces = false;

  public expandAllMonths = false;

  public _raceMonthMap: MonthRace = new MonthRace();

  public races: Race[];

  constructor(private raceService: RaceService,
              private _notificationService: NotificationService,
              private _raceMetaDataService: RaceMetaDataService,
              private _modalService: ModalService,
              private _raceAccordianService: RaceAccordianService) { }

  @ViewChildren(MonthAccordianComponent) accordians: QueryList<MonthAccordianComponent>;

  ngOnInit() {
    this._raceMetaDataService.loadedMetadata$.subscribe(this._handleInitialLoad);
    this.races = this.getRacesFromMetaData();
    this.filteredRaces = this.getRacesFromMetaData();
    this._raceMonthMap = this._raceMetaDataService.populateMonthRaces(this.getRacesFromMetaData());
  }

  private _handleInitialLoad: ((raceData: RaceDataModel) => void) = (raceData: RaceDataModel) => {
    this.races = raceData.races;
    this.filteredRaces = raceData.races;
    this._raceMonthMap = this._raceMetaDataService.populateMonthRaces(raceData.races);
  };

  @HostListener('window:scroll', ['$event']) onScrollEvent($event) {
    this._raceAccordianService.showAccordianSticky(this.accordians);
  }

  public expandMonths() {
    this._raceAccordianService.expandMonths(this.accordians);
  }

  public filterRacesEvent(filters: Filters) {
      this.raceService.findAll(filters).subscribe((races: Race[]) => {
        this.showFilteredRaces = true;

        if (this.isCountOnlyRequest(filters)) {
          this.filteredRaceCount = races.length;
        } else {
          this.filteredRaces = races;
          this.showFilteredRaces = true;
        }
      }, err => this.openErrorModal('Problem with the filter query'));
  }

  private isCountOnlyRequest(filters: Filters): boolean {
    return filters.getRaceCount;
  }

  public resetRacesAndCount(showAllRaces: boolean): void {
    this.filteredRaceCount = this.races.length;
    if (showAllRaces) {
      this.filteredRaces = this.races;
    }
  }

  public openAddRaceModal() {
    this._modalService.openAddRaceModal(this._raceMetaDataService.adaptLocationsToName(this.locations), this.terrains);
  }

  public openEditModal(race: Race) {
    // this._modalService.openEditModal(race, this.raceLocations);
  }

  public filterRacesByTypeAhead(filteredRaces: Race[]): void {
    this.filteredRaces = filteredRaces;
  }

  public displayFilteredRaces(showFilteredRaces: boolean): void {
    this.showFilteredRaces = showFilteredRaces;
  }

  public showFilters(): void {
    this.showColumnFilters = true;
    this.showFilteredRaces = true;
    this.filteredRaceCount = this.races.length;
    this.filteredRaces = [];
  }

  public hideFilters(): void {
    this.showColumnFilters = false;
    this.showFilteredRaces = false;
  }

  public handleRaceDelete(race: Race): void {
    this.raceService.deleteRace(race).subscribe(
      response => {
          this.updateRaces(race);
      },
      err => {
        this.openErrorModal()
      }
    );
  }

  private updateRaces(delRace: Race): void {
    this.races = this.races.filter((race: Race) => race.id !== delRace.id);
    this.updateMonthMapping(this.races);
    this.filteredRaces = this.races;
    this.filteredRaceCount = this.filteredRaces.length;
    this._raceMetaDataService.updateRaceMetadata();
  }

  private updateComponentAfterRacesUpdated(): void {

  }


  private updateMonthMapping(races: Race[]): void {
    this._raceMonthMap = this._raceMetaDataService.populateMonthRaces(races);
  }

  private openErrorModal(message?: string): void {
    this._notificationService.openErrorModal(message);
  }

  private getMonths(): string[] {
    return RaceMetaDataService.MONTHS;
  }

  get raceMonthMap(): MonthRace {
    return this._raceMonthMap;
  }
  
  get locations(): Location[] {
    return this._raceMetaDataService.getUpdatedLocationsCounts(this.races);
  }

  get terrains(): string[] {
    return this._raceMetaDataService.terrains.sort();
  }

  getRacesFromMetaData(): Race[] {
    return this._raceMetaDataService._races;
  }
}
