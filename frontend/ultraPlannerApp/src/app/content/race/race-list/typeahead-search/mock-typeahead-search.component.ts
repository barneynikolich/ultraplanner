import {Component, Input} from '@angular/core';
import {Race} from '../../models/race';

@Component({
  selector: 'app-typeahead-search',
  template: '<p>Mock Race List Table</p>'
})
export class MockTypeAheadSearchComponent {

  @Input()
  public races: Race[];
}
