import {Injectable} from '@angular/core';
import {Race} from '../../../models/race';

@Injectable()
export class TypeaheadFilterService {

  private static readonly MINIMUM_SEARCH_CHAR_LENGTH = 2;

  private _showFilteredRaces: boolean;

  public filterByTypeahead(filterTerm: string, races: Race[]): Race[] {
    if (filterTerm.length > TypeaheadFilterService.MINIMUM_SEARCH_CHAR_LENGTH) {
      this._showFilteredRaces = true;
      return this.filterRaceByTypeAhead(filterTerm, races);
    } else {
      this._showFilteredRaces = false;
      return races;
    }
  }

  // TODO: Maybe change logic to check for pattern rather than starts with
  private filterRaceByTypeAhead(filterTerm: string, races: Race[]): Race[] {
    return races.filter((race: Race) => {
      return race.name.toLowerCase().startsWith(filterTerm.toLowerCase());
    });
  }

  get showFilteredRaces(): boolean {
    return this._showFilteredRaces;
  }

}
