import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeaheadSearchComponent } from './typeahead-search.component';
import {NgbTypeaheadConfig, NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';

describe('TypeaheadSearchComponent', () => {
  let component: TypeaheadSearchComponent;
  let fixture: ComponentFixture<TypeaheadSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgbTypeaheadModule
      ],
      declarations: [ TypeaheadSearchComponent ],
      providers: [NgbTypeaheadConfig]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeaheadSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
