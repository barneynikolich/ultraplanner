import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Race} from '../../models/race';
import {TypeaheadFilterService} from './services/typeahead-filter.service';

@Component({
  selector: 'app-typeahead-search',
  templateUrl: './typeahead-search.component.html',
  styleUrls: ['./typeahead-search.component.css'],
  providers: [TypeaheadFilterService]
})
export class TypeaheadSearchComponent {

  private static readonly MAXIMUM_DROP_DOWN_LENGTH = 10;

  @Input()
  public races: Race[];

  @Output()
  public onFilterRaces = new EventEmitter<Race[]>();

  @Output()
  public onShowFilteredRaces = new EventEmitter<boolean>();

  constructor(private _typeaheadFilterService: TypeaheadFilterService) { }

  public typeAheadSearch = (text$: Observable<string>) => text$
    .debounceTime(100)
    .distinctUntilChanged()
    .map(term => term.length < 2 ? []
      : this.races.filter((race: Race) => race.name.toLowerCase()
        .indexOf(term.toLowerCase()) > -1)
        .slice(0, TypeaheadSearchComponent.MAXIMUM_DROP_DOWN_LENGTH))

  formatter = (x: Race) => x.name;

  public typeaheadClicked(event) {
    const query = (event.item as Race).name;
    if (query.length > 2) {
      this.filterRacesByInput(query);
    }
  }

  public filterRacesByInput(searchTerm: string): void {
    const filteredRaces: Race[] = this._typeaheadFilterService.filterByTypeahead(searchTerm, this.races);
    const showFilteredRaces: boolean = this._typeaheadFilterService.showFilteredRaces;

    this.onShowFilteredRaces.emit(showFilteredRaces);
    this.onFilterRaces.emit(filteredRaces);
  }

}
