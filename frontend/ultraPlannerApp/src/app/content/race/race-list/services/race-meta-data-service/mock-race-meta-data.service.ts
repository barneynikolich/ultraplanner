import {MonthRace} from '../../../../../models/monthRace';
import {Race} from '../../../models/race';
import {Observable} from "rxjs";

export class MockRaceMetaDataService {

  public populateMonthRaces(races: Race[]): MonthRace {
    return undefined;
  }

  public initialiseRaceMetadata(): Observable<any> {
    return undefined;
  }


  get racesUpdated$(): Observable<boolean> {
    return undefined;
  }

  get races(): Race[] {
    return undefined;
  }

}
