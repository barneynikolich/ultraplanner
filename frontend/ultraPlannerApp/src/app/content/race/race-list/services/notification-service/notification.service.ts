import {Injectable} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {Race} from '../../../models/race';
import {ErrorModalComponent} from '../../../../../error-modal/error-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Injectable()
export class NotificationService {
  constructor(public snackBar: MatSnackBar,
              private modalService: NgbModal) {}

  public openSaveSnackBar(race: Race): void {
    this.snackBar.open('Saved: ' + race.name.toUpperCase(), 'Dismiss', {
      duration: 2000,
    });
  }

  public openErrorModal(message?: string): void {
    const modalRef = this.modalService.open(ErrorModalComponent);
    modalRef.componentInstance.message = message;
  }

}
