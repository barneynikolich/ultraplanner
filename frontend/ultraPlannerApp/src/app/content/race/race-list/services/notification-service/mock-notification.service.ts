import {Race} from '../../../models/race';

export class MockNotificationService {

  public openSaveSnackBar(race: Race): void {
  }

  public openErrorModal(message?: string): void {
  }

}
