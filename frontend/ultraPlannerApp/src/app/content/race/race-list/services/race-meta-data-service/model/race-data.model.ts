import {Race} from "../../../../models/race";

export class RaceDataModel {

  private _locations: string[];
  private _races: Race[];
  private _terrains: string[];

  constructor(data: any[]) {
    this._locations = data[0];
    this.terrains = data[1];
    this.races = data[2];
  }

  get locations(): string[] {
    return this._locations;
  }

  set locations(value: string[]) {
    this._locations = value;
  }

  get terrains(): string[] {
    return this._terrains;
  }

  set terrains(value: string[]) {
    this._terrains = value;
  }

  get races(): Race[] {
    return this._races;
  }

  set races(value: Race[]) {
    this._races = value;
  }
}
