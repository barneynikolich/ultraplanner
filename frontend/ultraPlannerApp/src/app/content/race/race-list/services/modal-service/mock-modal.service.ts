import {Race} from '../../../models/race';

export class MockModalService {

  public openAddRaceModal(raceLocations: string[], raceTerrains: string[]) {
    return undefined;
  }

  private saveRace(race: Race): void {
  }

  public openEditModal(race: Race, raceLocations: string[]): void {
  }

}
