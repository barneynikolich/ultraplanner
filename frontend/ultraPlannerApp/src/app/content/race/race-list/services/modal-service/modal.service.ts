import {Injectable} from '@angular/core';
import {AddRaceModalComponent} from '../../../add-race-modal-component/add-race-modal-component';
import {Race} from '../../../models/race';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {RaceService} from '../../../services/race.service';
import {NotificationService} from '../notification-service/notification.service';
import { RaceMetaDataService } from '../race-meta-data-service/race-meta-data.service';

@Injectable()
export class ModalService {

  constructor (private _modalService: NgbModal,
               private _raceService: RaceService,
               private _notificationService: NotificationService,
               private _raceMetadataService: RaceMetaDataService) {}

  public openAddRaceModal(raceLocations: string[], raceTerrains: string[]) {
    const modalRef = this._modalService.open(AddRaceModalComponent);
    modalRef.componentInstance.raceLocations =  raceLocations;
    modalRef.componentInstance.terrains = raceTerrains;
    modalRef.componentInstance.onFormSubmit.subscribe((submittedRace: Race) => {
      this.saveRace(submittedRace);
    });
  }

  private saveRace(race: Race) {
    this._raceService.saveRace(race).subscribe((result: Race) => {
      this._raceMetadataService.updateRaces();
      this._notificationService.openSaveSnackBar(result);
    }, err => this._notificationService.openErrorModal());
  }

  public openEditModal(race: Race, raceLocations: string[]) {
    const modalRef = this._modalService.open(AddRaceModalComponent);
    modalRef.componentInstance.raceToEdit = race;
    modalRef.componentInstance.raceLocations = raceLocations;
    modalRef.componentInstance.onFormSubmit.subscribe((submittedRace: Race) => {
      this._raceService.editRace(submittedRace).subscribe((res) => {
        // TODO Must get races from server or save wont upload whats there!
        // this.getRacesFromServer();
      }, error => this._notificationService.openErrorModal());
    });
  }

}
