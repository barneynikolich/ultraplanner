import {Injectable} from '@angular/core';
import {Race} from '../../../models/race';
import {MonthRace} from '../../../../../models/monthRace';
import {Month} from '../../../../../../../../../../ultra-planner/src/app/race-search/models/Month';
import {Location} from "../../model/location";
import {RaceService} from "../../../services/race.service";
import {Observable, Subject} from "rxjs";
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {of} from "rxjs/observable/of";
import {ProgressBarService} from "../../../../services/progress-bar.service";
import {ProgressEvent} from "../../../../services/progress-bar-event.enum";
import {RaceDataModel} from "./model/race-data.model";

@Injectable()
export class RaceMetaDataService {

  public static readonly MONTHS = [Month.JANUARY, Month.FEBRUARY, Month.MARCH, Month.APRIL, Month.MAY, Month.JUNE, Month.JULY,
    Month.AUGUST, Month.SEPTEMBER, Month.OCTOBER, Month.NOVEMBER, Month.DECEMBER];

  private _locations: string[] = [];
  public _races: Race[] = [];
  public _terrains: string[] = [];

  private _racesUpdated: BehaviorSubject<any> = new BehaviorSubject({});
  private _racesUpdated$: Observable<boolean> = this._racesUpdated.asObservable();

  private _loadedMetadata: Subject<object> = new Subject();
  private _loadedMetadata$: Observable<any> = this._loadedMetadata.asObservable();

  constructor(private _raceService: RaceService, private _progressBarService: ProgressBarService) {
    this.racesUpdated$.subscribe(() => {
      console.log('[MetadataService] = Updating metadata ')
      this.initialiseRaceMetadata();
    });
  }

  public initialiseRaceMetadata(): void {
    this._progressBarService.showProgressBar(ProgressEvent.LOADING_ALL_RACES);
    let response1 = this._raceService.getLocations().map((locations: string[]) => this._locations = locations);
    let response2 = this._raceService.getTerrains().map((terrains: string[]) => this._terrains = terrains);
    let response3 = this._raceService.findAll().map((races: Race[]) => this._races = races);

    Observable.forkJoin([response1, response2, response3]).subscribe((response: any) => {
      this._loadedMetadata.next(new RaceDataModel(response));
      this._progressBarService.hideProgressBar();
    });
  }

  public getRaces$(): Observable<Race[]> {
      if (this._races.length > 0) {
        console.log('Races in Cache')
        return of(this._races);
      } else {
        console.log('Races from server')
        return this._raceService.findAll().map((races: Race[]) => this._races = races);
      }
  }

  public getLocations$(): Observable<string[]> {
    if (this._locations.length > 0) {
      console.log('Races in Cache')
      return of(this._locations);
    } else {
      console.log('Races from server')
      return this._raceService.getLocations().map((locations: string[]) => this._locations = locations);
    }
  }

  public getTerrains$(): Observable<string[]> {
    if (this._terrains.length > 0) {
      console.log('Races in Cache')
      return of(this._terrains);
    } else {
      console.log('Races from server')
      return this._raceService.getTerrains().map((terrains: string[]) => this._terrains = terrains);
    }
  }

  public updateRaces(): void {
    this._racesUpdated.next(true);
  }

  public updateRaceMetadata(): void {
    this._raceService.findAll().subscribe((races: Race[]) => {
      this._races = races;
      this._locations = this.adaptLocationsToName(this.getUpdatedLocationsCounts(races));
    });
  }

  public populateMonthRaces(races: Race[]): MonthRace {
    const raceMonthMap: MonthRace = new MonthRace();

    RaceMetaDataService.MONTHS.forEach((month: string) => {
      races.forEach((race: Race) => {

        const monthShort = month.slice(0, 3);
        const raceDateMonthShort: string = race.date.slice(7, 10);
        if (monthShort === raceDateMonthShort) {
          raceMonthMap.toggleRace(month, race);
        }
      });
    });
    return raceMonthMap;
  }

  public getUpdatedLocationsCounts(races: Race[]): Location[] {
    return this.adaptToLocations(this._locations, races);
  }

  public adaptLocationsToName(locations: Location[]): string[] {
    return locations.map(location => location.name);
  }

  private populateLocationCount(location: string, races: Race[]): number {
    return races.filter(race => race.area.toLowerCase() == location.toLowerCase()).length;
  }

  private adaptToLocations(locations: string[], races: Race[]): Location[] {
    return locations.sort().map((location) => new Location(location, this.populateLocationCount(location, races)));
  }

  get terrains(): string[] {
    return this._terrains;
  }

  get racesUpdated$(): Observable<boolean> {
    return this._racesUpdated$;
  }

  get loadedMetadata$(): Observable<any> {
    return this._loadedMetadata$;
  }

  get races(): Race[] {
    return this._races;
  }

}
