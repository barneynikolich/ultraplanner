import {Injectable} from '@angular/core';
import {Filters} from '../../../../../models/filters';
import {Race} from '../../../models/race';
import {RaceService} from '../../../services/race.service';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class RaceFilterService {

  private filteredRaces: Race[];

  private _showFilteredRaces: boolean;

  constructor(private _raceService: RaceService) {}


  public filterRacesEventFired(filters: Filters, filteredRaceCount: number, filteredRaces: Race[], showFilteredRaces: boolean) {

    this._raceService.findAll(filters).subscribe((races: Race[]) => {

      if (this.isCountOnlyRequest(filters)) {
        filteredRaceCount = races.length;
      } else {
        filteredRaces = races;
        showFilteredRaces = true;
      }
    });
    // , err => this.openErrorModal('Problem with the filter query'));
  }

  public isCountOnlyRequest(filters: Filters): boolean {
    return filters.getRaceCount;
  }

  // public getFilteredRaceCount(filters): number {
  //   this._raceService.findAll(filters).subscribe((races: Race[]) => {
  //     return races.length;
  //   });
  // }

  public getFilteredRaces(filters): Race[] {
    this._raceService.findAll(filters).subscribe((races: Race[]) => {
      console.log(races)
      this.filteredRaces = races;
    });

    console.log('filteredRaces: ', this.filteredRaces);
    return this.filteredRaces;
  }


  get showFilteredRaces(): boolean {
    return this._showFilteredRaces;
  }
}
