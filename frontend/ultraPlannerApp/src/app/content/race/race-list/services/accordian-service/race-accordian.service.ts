
import {Injectable, QueryList} from '@angular/core';
import {MonthAccordianComponent} from '../../month-accordian/month-accordian.component';

@Injectable()
export class RaceAccordianService {

  private expandAllMonths = false;

  public expandMonths(accordians: QueryList<MonthAccordianComponent>): void {
    this.expandAllMonths = !this.expandAllMonths;
    if (this.expandAllMonths) {
      accordians.forEach((accordian: MonthAccordianComponent) => {
        accordian.expanded = true;
      });
    } else {
      accordians.forEach((accordian: MonthAccordianComponent) => {
        accordian.expanded = false;
      });
    }
  }

  public showAccordianSticky(accordians: QueryList<MonthAccordianComponent>): void {
    accordians.forEach((accordian: MonthAccordianComponent) => {
      if (this.scrollIsWithinMonthAccordian(accordian)) {
        accordian.sticky = true;
      } else {
        accordian.sticky = false;
      }
    });
  }

  private scrollIsWithinMonthAccordian(accordian: MonthAccordianComponent): boolean {
    const scrollPosition = window.pageYOffset;
    const topOfAccordian = accordian.offSetTop;
    const bottomOfAccordian = accordian.offSetBottom;
    const accordianHeight = topOfAccordian + bottomOfAccordian;

    return scrollPosition >= topOfAccordian && scrollPosition <= accordianHeight;
  }

  public getExpandAllMonths() {
    return this.expandAllMonths;
  }
}
