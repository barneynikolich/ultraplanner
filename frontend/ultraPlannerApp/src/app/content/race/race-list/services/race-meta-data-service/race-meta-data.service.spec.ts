import { TestBed } from '@angular/core/testing';
import {RaceMetaDataService} from './race-meta-data.service';
import {Race} from '../../../models/race';
import {Month} from '../../../../../../../../../../ultra-planner/src/app/race-search/models/Month';
import {RaceService} from "../../../services/race.service";
import {LOCATIONS, RACES, TERRAINS} from "../../../../../test-constants/test-constants";

describe('RaceMetaDataService spec', () => {

  let serviceUnderTest: RaceMetaDataService;

  const mockRaceService: {getTerrains: jasmine.Spy, getLocations: jasmine.Spy, findAll: jasmine.Spy} =
    jasmine.createSpyObj('MockRaceService', ['getTerrains', 'getLocations', 'findAll']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        RaceMetaDataService,
        {provide: RaceService, useValue: mockRaceService},
      ],
    });
    resetSpies();
    serviceUnderTest = TestBed.get(RaceMetaDataService);
  });

  it('should initialise race data', () => {
    mockRaceService.getTerrains.and.returnValue(TERRAINS());
    mockRaceService.getLocations.and.returnValue(LOCATIONS());
    mockRaceService.findAll.and.returnValue(RACES());

    serviceUnderTest.initialiseRaceMetadata().subscribe((response: any) => {
      console.log(response)
    });

  });

  it('should return an object containing races mapped to the month they are in', () => {
    const response = serviceUnderTest.populateMonthRaces(RACES());

    expectLength(response.findColumnKeys(Month.JANUARY), 1);
    expectLength(response.findColumnKeys(Month.FEBRUARY), 1);
    expectLength(response.findColumnKeys(Month.MARCH), 2);

    expect(response.findColumnKeys(Month.MARCH)[0].name).toBe('Race_3');
  });

  function expectLength(races: Race[], expectedLength: number): void {
    expect(races.length).toBe(expectedLength);

  }

  function resetSpies(): void {
    mockRaceService.findAll.calls.reset();
    mockRaceService.getLocations.calls.reset();
    mockRaceService.getTerrains.calls.reset();
  }

});
