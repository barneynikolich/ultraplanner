export class Race {

  id: number;
  name: string;
  date: any;
  area: string;
  miles: number;
  mclimb: number;
  terrain: string;
  km: string;
  mkm: string;
  eventType: string;
  cost: string;
  costMile: string;
  notes: string;
  website: string;

  constructor(
              id: number,
              name: string,
              date: any,
              area: string,
              miles: any,
              mclimb: any,
              terrain: any,
              km: string,
              mkm: string,
              eventType: string,
              cost: string,
              costMile: string,
              notes: string,
              website: string) {
    this.id = id;
    this.name = name;
    this.date = date;
    this.area = area;
    this.miles = miles;
    this.mclimb = mclimb;
    this.terrain = terrain;
    this.km = km;
    this.mkm = mkm;
    this.eventType = eventType;
    this.cost = cost;
    this.costMile = costMile;
    this.notes = notes;
    this.website = website;
  }


}
