import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpcomingComponent } from './upcoming.component';
import {NgMaterialModule} from '../../../ng-material/ng-material.module';
import {MockRaceService} from '../services/mock-race.service';
import {RaceService} from '../services/race.service';
import {MockRaceListTableComponent} from '../race-list/race-list-table/mock-race-list-table.component';

describe('UpcomingComponent', () => {
  let component: UpcomingComponent;
  let fixture: ComponentFixture<UpcomingComponent>;
  let raceService: MockRaceService;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [NgMaterialModule],
      declarations: [ UpcomingComponent, MockRaceListTableComponent ],
      providers: [
        {provide: RaceService, useClass: MockRaceService}
        ]
    })
    .compileComponents();

    raceService = TestBed.get(RaceService);

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpcomingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
