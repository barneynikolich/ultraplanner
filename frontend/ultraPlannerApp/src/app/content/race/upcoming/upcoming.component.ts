import {Component, OnDestroy, OnInit} from '@angular/core';
import {Race} from '../models/race';
import {RaceService} from '../services/race.service';
import {NgbAlertConfig} from '@ng-bootstrap/ng-bootstrap';
import {Subscription} from 'rxjs/Subscription';

@Component({
  selector: 'app-upcoming',
  templateUrl: './upcoming.component.html',
  styleUrls: ['./upcoming.component.css'],
  providers: [NgbAlertConfig]
})
export class UpcomingComponent implements OnInit, OnDestroy {

  public races: Race[];
  public monthName: string;
  public showProgressBar = false;
  private upcomingRacesSubscription: Subscription;

  constructor(private raceService: RaceService) { }

  ngOnInit() {
    this.showProgressBar = true;
    if (this.raceService.upcomingRaces) {
      this.races = this.raceService.upcomingRaces;
      this.showProgressBar = false;
    } else {
      this.upcomingRacesSubscription = this.raceService.getUpcomingRaces().subscribe(
        (res: Race[]) => {
          this.races = res;
        }, err => console.log(err),
        () => {
          this.showProgressBar = false;
        });
    }

    this.monthName = this.getMonthName(new Date().getMonth());
  }

  ngOnDestroy() {
    if (!this.raceService.upcomingRaces) {
      this.upcomingRacesSubscription.unsubscribe();
    }
  }

  private getMonthName(month: number): string {
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July',
      'August', 'September', 'October', 'November', 'December'];
    return months[month];
  }

}
