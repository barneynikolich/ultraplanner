import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RaceListComponent} from './race-list/race-list.component';
import {RaceDetailComponent} from './race-list/race-detail/race-detail.component';
import {UpcomingComponent} from './upcoming/upcoming.component';


const routes: Routes = [
  // {path: '', component: RaceListComponent},
  // {path: 'races', component: RaceListComponent},
  // {path: 'upcoming', component: UpcomingComponent},
  // {path: 'racedetail/:name', component: RaceDetailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RaceRoutingModule { }
