
import {Component, OnInit, Inject, Input, Output, EventEmitter} from '@angular/core';
import {NgbActiveModal, NgbTypeahead, NgbTypeaheadConfig} from '@ng-bootstrap/ng-bootstrap';
import {Race} from '../models/race';
import {FormBuilder, FormGroup, Validators, FormControl, AbstractControl} from '@angular/forms';
import {Observable} from 'rxjs/Observable';


@Component({
  selector: 'app-add-race-modal-component',
  templateUrl: './add-race-modal-component.html',
  styleUrls: ['./add-race-modal-component.css'],
  providers: [FormBuilder, NgbTypeahead, NgbTypeaheadConfig]
})
export class AddRaceModalComponent implements OnInit {

  @Input()
  public raceToEdit: Race;

  public addRaceForm: FormGroup;
  public showFormErr = false;

  @Input()
  public raceLocations: string[];

  @Input()
  public terrains: string[];

  @Output()
  onFormSubmit: EventEmitter<Race> = new EventEmitter();

  constructor(public activeModal: NgbActiveModal, private formBuilder: FormBuilder) {
    this.createAddRaceForm();
  }

  ngOnInit() {
    if (this.isEditForm()) {
      this.setEditRaceFormValues();
    }
  }

  terrainSearch = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 0 ? []
        : this.terrains.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))

  search = (text$: Observable<string>) =>
    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .map(term => term.length < 0 ? []
        : this.raceLocations.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))

  private setEditRaceFormValues(): void {

    this.addRaceForm.setValue({
      name: this.raceToEdit.name,
      date: this.raceToEdit.date,
      miles: this.raceToEdit.miles,
      km: this.raceToEdit.km,
      area: this.raceToEdit.area,
      terrain: this.raceToEdit.terrain,
      mclimb: this.raceToEdit.mclimb,
      website: this.raceToEdit.website
    });
  }

  private createAddRaceForm() {
    this.addRaceForm = this.formBuilder.group({
      name:  ['', Validators.required ],
      date: ['', Validators.required ],
      miles: '',
      km: '',
      area: '',
      terrain: ['', Validators.required],
      mclimb: '',
      website: ''
    });
  }

  public onAddRaceSubmit(model) {
    console.log('Add race called:');
    if (this.addRaceForm.invalid) {
      this.showFormErr = true;
    } else {
      const race = model as Race;
      this.onFormSubmit.emit(race);
      this.addRaceForm.reset();
      this.activeModal.dismiss();
    }
  }

  public editRace(model) {
    if (this.addRaceForm.invalid) {
      this.showFormErr = true;
    } else {
      const race = model as Race;
      race.id = this.raceToEdit.id;
      this.onFormSubmit.emit(race);
      this.addRaceForm.reset();
      this.activeModal.dismiss();
    }
  }

  milesEntered() {
    const miles = this.addRaceForm.get('miles').value;
    const km = (miles * 1.609344).toPrecision(7 );
    this.addRaceForm.get('km').setValue(km);
  }

  kmEntered() {
    const km = this.addRaceForm.get('km').value;
    const miles = (km * 0.62137119).toPrecision(7);
    this.addRaceForm.get('miles').setValue(miles);
  }

  public isEditForm(): boolean {
    if (this.raceToEdit) {
      return true;
    } else {
      return false;
    }
  }

}
