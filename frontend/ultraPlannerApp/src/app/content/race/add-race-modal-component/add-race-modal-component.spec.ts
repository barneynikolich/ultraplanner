import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddRaceModalComponent } from './add-race-modal-component';
import {ReactiveFormsModule} from '@angular/forms';
import {NgbActiveModal, NgbTypeaheadModule} from '@ng-bootstrap/ng-bootstrap';

describe('AddRaceModalComponentComponent', () => {
  let component: AddRaceModalComponent;
  let fixture: ComponentFixture<AddRaceModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule, NgbTypeaheadModule],
      declarations: [ AddRaceModalComponent ],
      providers: [NgbActiveModal]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddRaceModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
