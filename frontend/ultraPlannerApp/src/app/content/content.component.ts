import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import 'rxjs/add/operator/finally';
import {AuthService} from '../authentication/AuthService';
import { ProgressBarService } from './services/progress-bar.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  private _showProgressBarSubscription: Subscription;

  public showProgressBar: boolean;

  constructor(private http: HttpClient,
              private router: Router,
              private auth: AuthService,
              private _progressBarService: ProgressBarService) { }

  ngOnInit() {
    this._showProgressBarSubscription = this._progressBarService.showProgressBar$.subscribe((showProgressBar: boolean) => {
      this.showProgressBar = showProgressBar;
    });
  }
}
