import {Observable} from 'rxjs/Observable';
import {ProgressEvent} from './progress-bar-event.enum';

export class MockProgressBarService {
  public showProgressBar(eventType: ProgressEvent = ProgressEvent.LOADING_ALL_RACES): void {
  }

  public hideProgressBar(): void {
  }

  get showProgressBar$(): Observable<boolean> {
    return Observable.of();
  }
}
