import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { ProgressEvent } from './progress-bar-event.enum';

@Injectable()
export class ProgressBarService {

  private _showProgressBar: BehaviorSubject<boolean> = new BehaviorSubject(false);
  private _showProgressBar$: Observable<boolean> = this._showProgressBar.asObservable();

  public showProgressBar(eventType: ProgressEvent = ProgressEvent.LOADING_ALL_RACES): void {
    this._showProgressBar.next(true);
  }

  public hideProgressBar(): void {
    this._showProgressBar.next(false);
  }

  get showProgressBar$(): Observable<boolean> {
    return this._showProgressBar$;
  }
}
