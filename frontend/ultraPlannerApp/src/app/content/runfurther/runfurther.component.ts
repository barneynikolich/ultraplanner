import { Component, OnInit } from '@angular/core';
import { RunFurtherRace } from '../../models/runFurtherRace';

@Component({
  selector: 'app-runfurther',
  templateUrl: './runfurther.component.html',
  styleUrls: ['./runfurther.component.css']
})
export class RunfurtherComponent implements OnInit {

  public selectedRace: RunFurtherRace;
  public runFurtherRaces: RunFurtherRace [];

  constructor() {
    this.runFurtherRaces = this.setUpRaces();
  }

  ngOnInit() {
  }

  raceSelected(raceName: RunFurtherRace): void {
    console.log('Race selected: ', raceName);
    this.selectedRace = raceName;
  }

  public highlightRow(name: string): boolean {
    if (this.selectedRace && (name === this.selectedRace.name)) {
      return true;
    } else {
      return false;
    }
  }

  private setUpRaces(): RunFurtherRace[] {
    const haworth: RunFurtherRace = {
      name: 'Haworth Hobble',
      date: 'Sat 10 March',
      area: 'West Yorkshire',
      distance: '32',
      notes: 'The Haworth Hobble has been a regular Runfurther event for a long time: it attracts more Runfurther ' +
      'runners than any other event. Its 32 miles follow Yorkshire Pennine paths and tracks from Haworth over the ' +
      'moors to Calderdale, up Stoodley Pike and back to Haworth via Heptonstall and Crimsworth Dean.  The views are great, ' +
      'and it’s nearly all runnable apart from the climb up the Pike, provided you’ve got enough left in the tank!  ' +
      'This race sorts out who’s taken it easy over the winter with a vengeance.  Better get training then…',
      ascent: '4400 ft',
      website: 'http://kcac.co.uk/events/haworth-hobble/',
      category: 'S'
     };

    const lakes42: RunFurtherRace = {
      name: 'Lakes 42',
      date: 'Sat 31 March',
      area: 'Lake District',
      distance: '42',
      notes: 'This race is one of Joe Faulkner’s excellent NAV4 events, this one in the eastern Lake District and' +
      ' taking in the summits of Loadpot Hill, Helvellyn, Whiteside and Place Fell.  If you’ve run the Tour de Helvellyn' +
      ' winter race you’ll be familiar with some of the route, but this one goes to the summits rather than crossing the' +
      ' passes.  There could be snow and ice, as long as global warming holds off at the end of this winter.',
      ascent: '10000 ft',
      website: 'http://kcac.co.uk/events/haworth-hobble/',
      category: 'S'
    };


    const calderdale: RunFurtherRace = {
      name: 'Calderdale Hike',
      date: 'Sat 14 April',
      area: 'West Yorkshire',
      distance: '40',
      notes: 'This classic Yorkshire Pennines trail event always attracts a lot of Runfurther runners.  It starts at Sowerby' +
      ' in the Calder Valley, taking a big loop around Calderdale, with a new route every three years.  2018 will be a new route' +
      ' including Ripponden, Lumbutts and Heptonstall and as always a lot of varied moorland and valley scenery.  The route is ' +
      'defined only by the checkpoints, and you must devise your own route between them.  Many runners lost time routefinding in' +
      ' 2015 when there was last a new route, so plan ahead!',
      ascent: '6600 ft',
      website: 'http://kcac.co.uk/events/haworth-hobble/',
      category: 'M'

    };

    return [haworth, lakes42, calderdale];
  }




}
