import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RunfurtherComponent } from './runfurther.component';
import { MatCardModule } from '@angular/material';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-race-info',
  template: '<p>Mock app-race-info</p>'
})
class MockAppRaceInfoComponent {
  @Input()
  race: Object;
}

describe('RunfurtherComponent', () => {
  let component: RunfurtherComponent;
  let fixture: ComponentFixture<RunfurtherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RunfurtherComponent, MockAppRaceInfoComponent ],
      imports: [MatCardModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RunfurtherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
