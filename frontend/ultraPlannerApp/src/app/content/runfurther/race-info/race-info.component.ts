import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-race-info',
  templateUrl: './race-info.component.html',
  styleUrls: ['./race-info.component.css']
})
export class RaceInfoComponent implements OnInit {

  @Input()
  race: Object;

  constructor() {
  }

  ngOnInit() {
  }

  public hidePrompt(): boolean {
    return this.race ? false : true;
  }

}
