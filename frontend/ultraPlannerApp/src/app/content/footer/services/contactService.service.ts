import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {ContactRequest} from '../models/contact-request.modal';

@Injectable()
export class ContactService {

  private baseUrl = environment.localhost;

  constructor(private _httpClient: HttpClient) {}

  public saveContactRequest(contactRequest: ContactRequest): Observable<Object> {
    return this._httpClient.post(this.baseUrl + 'contactRequest', this.constructContactRequest(contactRequest))
      .catch((error: any) => Observable.throw(error.json()
      .error || 'Server error'));
  }

  private constructContactRequest(contactRequest: ContactRequest): object {
    return {
        firstName: contactRequest.firstName,
        lastName: contactRequest.lastName,
        email: contactRequest.email,
        message: contactRequest.message,
        date: contactRequest.date
    };
  }


}
