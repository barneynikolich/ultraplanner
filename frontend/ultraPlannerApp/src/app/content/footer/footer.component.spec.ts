import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterComponent } from './footer.component';
import { MatSnackBarModule } from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ContactService} from './services/contactService.service';
import {MockContactService} from './services/mock-contact.service';
import {RaceModule} from '../race/race.module';

describe('FooterComponent', () => {
  let component: FooterComponent;
  let fixture: ComponentFixture<FooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FooterComponent ],
      imports: [
        MatSnackBarModule,
        FormsModule,
        ReactiveFormsModule,
      ]
    }).overrideComponent(FooterComponent, {
      set: {
        providers: [
          { provide: ContactService, useClass: MockContactService }
        ]
      }
    }).compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
