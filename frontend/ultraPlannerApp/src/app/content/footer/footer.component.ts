import { Component, OnInit } from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ContactRequest} from './models/contact-request.modal';
import {ContactService} from './services/contactService.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
  providers: [ContactService]
})
export class FooterComponent {

  public showContactForm = false;

  public toggleContactForm(): void {
    this.showContactForm = !this.showContactForm;
  }

}
