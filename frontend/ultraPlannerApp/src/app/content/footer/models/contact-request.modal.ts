export class ContactRequest {


  constructor(private _firstName: string, private _lastName, private _email: string, private _message, private _date: Date) {}

  get firstName(): string {
    return this._firstName;
  }

  set firstName(value: string) {
    this._firstName = value;
  }

  get lastName() {
    return this._lastName;
  }

  set lastName(value) {
    this._lastName = value;
  }

  get email(): string {
    return this._email;
  }

  set email(value: string) {
    this._email = value;
  }

  get message() {
    return this._message;
  }

  set message(value) {
    this._message = value;
  }


  get date(): Date {
    return this._date;
  }

  set date(value: Date) {
    this._date = value;
  }
}
