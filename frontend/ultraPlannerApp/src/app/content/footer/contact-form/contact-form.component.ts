import {Component, OnInit, Input, OnChanges, OnDestroy} from '@angular/core';
import {MatSnackBar} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ContactRequest} from '../models/contact-request.modal';
import {ContactService} from '../services/contactService.service';
import {fadeInFadeOut} from '../../../../../../../../ultra-planner/src/app/animations';

@Component({
  selector: 'contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css'],
  animations: [fadeInFadeOut]
})
export class ContactFormComponent implements OnDestroy {

  @Input()
  public isVisible: boolean;

  public contactMeForm: FormGroup;

  constructor(public snackBar: MatSnackBar, private formBuilder: FormBuilder, private _contactService: ContactService) {
    this.createContactMeForm();
  }

  ngOnDestroy(): void {
    console.log('component destroyed!')
  }


 
  public onSubmit(modal: ContactRequest): void {
    const contactRequest: ContactRequest = new ContactRequest(modal.firstName, modal.lastName, modal.email, modal.message, new Date())
    this._contactService.saveContactRequest(contactRequest).subscribe(() => {
      this.openSuccessSnackBar();
    });

    this.resetContactForm();
  }

  private resetContactForm(): void {
    this.contactMeForm.reset();
  }

  private createContactMeForm(): void {
    this.contactMeForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', Validators.email],
      message: ''
    });
  }

  private openSuccessSnackBar(): void {
    this.snackBar.open('Message sent.', 'Dismiss', {
      duration: 3000,
    });
  }

}
