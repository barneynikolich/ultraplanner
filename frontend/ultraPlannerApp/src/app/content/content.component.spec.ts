// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
//
// import { ContentComponent } from './content.component';
// import {RouterTestingModule} from '@angular/router/testing';
// import { Component } from '@angular/core';
// import {MatProgressBarModule} from '@angular/material';
// import {HttpClientTestingModule} from '@angular/common/http/testing';
// import {AuthService} from '../authentication/AuthService';
// import {MockAuthService} from '../authentication/mock-auth.service';
// import {ProgressBarService} from './services/progress-bar.service';
// import {MockProgressBarService} from './services/mock-progress-bar.service';
//
// @Component({
//   selector: 'app-footer',
//   template: '<p>Mock app footer</p>'
// })
// class MockAppFooterComponent {
// }
//
// describe('ContentComponent', () => {
//   let component: ContentComponent;
//   let fixture: ComponentFixture<ContentComponent>;
//
//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       imports: [
//         RouterTestingModule,
//         MatProgressBarModule,
//         HttpClientTestingModule
//       ],
//       declarations: [ ContentComponent, MockAppFooterComponent ],
//       providers: [
//         { provide: AuthService, useClass: MockAuthService },
//         { provide: ProgressBarService, useClass: MockProgressBarService }
//       ]
//     })
//     .compileComponents();
//   }));
//
//   beforeEach(() => {
//     fixture = TestBed.createComponent(ContentComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });
//
//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
// });
