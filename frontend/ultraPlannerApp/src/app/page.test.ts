import { DebugElement, Type } from '@angular/core';
import { By } from '@angular/platform-browser';

export abstract class Page {

  constructor(private _debugElement: DebugElement) {}

  public abstract updatePage();

  protected getHtmlElement(css: string, debugElement: DebugElement = this._debugElement): HTMLElement {
    const el: DebugElement = debugElement.query(By.css(css));
    if (el != null) {
      return el.nativeElement;
    }
  }

  protected getHtmlElements(css: string, debugElement: DebugElement = this._debugElement): HTMLElement[] {
    return debugElement.nativeElement.querySelectorAll(css);
  }

  protected getSVGElement(css: string, debugElement: DebugElement = this._debugElement): SVGElement {
    return debugElement.nativeElement.querySelector(css);
  }

  protected getComponent<T>(css: string): T {
    const componentElement = this.debugElement.query(By.css(css));
    if (componentElement != null) {
      return componentElement.componentInstance as T;
    } else {
      return undefined;
    }
  }

  protected getComponents<T>(css: string): T[] {
    const componentElements: DebugElement[] = this.debugElement.queryAll(By.css(css));
    if (componentElements != null) {
      const components: T[] = [];
      for (const debugElement of componentElements) {
        components.push(debugElement.componentInstance);
      }
      return components;
    } else {
      return undefined;
    }
  }

  protected getDirective<T>(directive: Type<any>): T {
    const directiveElement = this.debugElement.query(By.directive(directive));
    if (directiveElement != null) {
      return directiveElement.injector.get(directive) as T;
    } else {
      return undefined;
    }
  }

  get debugElement(): DebugElement {
    return this._debugElement;
  }
}
