import {AfterViewInit, Component, OnInit} from '@angular/core';
import { NgbTabset, NgbTabsetConfig } from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {RaceMetaDataService} from "./content/race/race-list/services/race-meta-data-service/race-meta-data.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [NgbTabsetConfig]
})
export class AppComponent implements OnInit, AfterViewInit {

  public activeTabId: string;
  public RACES_COMPONENT_URL = 'races';
  public TEST_COMPONENT_URL = 'test';
  public showFooter = false;

  constructor(private router: Router, private _raceMetadataService: RaceMetaDataService) {}

  public ngOnInit() {
    // this.setActiveTab();
    this._raceMetadataService.initialiseRaceMetadata();
  }

  public ngAfterViewInit() {
    // setTimeout(() => {
    //   this.showFooter = true;
    // }, 5000);
  }

  public navigate(event: any, t: NgbTabset) {
    const url = event.nextId;
    this.activeTabId = event.nextId;
    this.router.navigate([url]);
  }

  private setActiveTab() {
    const url = window.location.href;

    if (url.endsWith(this.RACES_COMPONENT_URL)) {
      this.activeTabId = this.RACES_COMPONENT_URL;
    } else if (url.endsWith(this.TEST_COMPONENT_URL)) {
      this.activeTabId = this.TEST_COMPONENT_URL;
    }
  }

}
