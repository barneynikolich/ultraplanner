export class RunFurtherRace {

  // id: number;
  name: string;
  date: any;
  area: string;
  distance: string;
  ascent: string;
  notes: string;
  website: string;
  category: string;

  constructor(
    // id: number,
    name: string,
    date: string,
    area: string,
    distance: string,
    ascent: any,
    notes: string,
    website: string) {

    // this.id = id;
    this.name = name;
    this.date = date;
    this.area = area;
    this.distance = distance;
    this.ascent = ascent;
    this.notes = notes;
    this.website = website;
  }


}
