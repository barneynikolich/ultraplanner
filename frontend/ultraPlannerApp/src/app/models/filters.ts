export class Filters {

  public static readonly AREA = 'area';
  public static readonly TERRAIN = 'terrain';
  public static readonly LOCATION = 'location';
  public static readonly DATE = 'dateSort';

  private _filters: object = {};

  constructor(private _getRaceCount = false) {
  }

  public addFilter(key: string, value: string) {
    if (value) {
      this._filters[key] = value;
    }
  }

  public reset(): void {
    this._filters = [];
  }

  get filters(): object {
    return this._filters;
  }

  get getRaceCount(): boolean {
    return this._getRaceCount;
  }

  set getRaceCount(value: boolean) {
    this._getRaceCount = value;
  }
}
