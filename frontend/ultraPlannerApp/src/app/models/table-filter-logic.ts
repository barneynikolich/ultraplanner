export class TableFilterLogic {

  public static readonly distanceOptions = ['< 26', '26 - 49', '50 - 69', '70 - 99', '> 100'];

  constructor(private _distance: string, private _area: string, private _terrain: string, private _getRaceCount = false,
              private _dateSort: string) {
  }

  get distance(): string {
    return this._distance;
  }

  get area(): string {
    return this._area;
  }

  get terrain(): string {
    return this._terrain;
  }

  get dateSort(): string {
    return this._dateSort;
  }

  set dateSort(value: string) {
    this._dateSort = value;
  }
}
