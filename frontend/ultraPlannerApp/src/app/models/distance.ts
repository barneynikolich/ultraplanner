export class Area {

  constructor (private _area: string, private _count: number ) {}

  get area(): string {
    return this._area;
  }

  set area(value: string) {
    this._area = value;
  }

  get count(): number {
    return this._count;
  }

  set count(value: number) {
    this._count = value;
  }
}
