import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal, NgbAlertConfig} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-error-modal',
  templateUrl: './error-modal.component.html',
  styleUrls: ['./error-modal.component.css'],
  providers: [NgbAlertConfig]
})
export class ErrorModalComponent implements OnInit {

  @Input()
  public message: string;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
