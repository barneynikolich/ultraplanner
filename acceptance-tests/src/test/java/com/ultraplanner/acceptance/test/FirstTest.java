package com.ultraplanner.acceptance.test;

import com.ultraplanner.acceptance.test.enums.BrowserType;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class FirstTest {

    private static WebDriver driver;

    @Before
    public void setUp() {
        System.setProperty(BrowserType.CHROME.getPropertyKey(), BrowserType.CHROME.getPropertyValue());
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--test-type --start-maximized --ignore-certificate-errors --disable-infobars");
        driver = new ChromeDriver();
    }

    @Test
    public void checkPageTitle() {
        startWebdriver();
        Assert.assertTrue("Title should be UltraPlannerApp",
                driver.getTitle().equals("UltraPlannerApp"));
    }


    public void startWebdriver() {
        driver.navigate().to("http://178.62.34.171");
    }

    @After
    public void finishTests() {
        driver.close();
    }
}
