package com.ultraplanner.acceptance.test.enums;

public enum BrowserType {
    CHROME("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver.exe"),
    FIREFOX("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver.exe"),
    IE("webdriver.ie.driver", "src/test/resources/drivers/IEDriverServer.exe");

    private String propertyKey;
    private String propertyValue;

    BrowserType(String propertyKey, String propertyValue)
    {
        this.propertyKey = propertyKey;
        this.propertyValue = propertyValue;
    }

    public String getPropertyKey()
    {
        return this.propertyKey;
    }

    public String getPropertyValue()
    {
        return this.propertyValue;
    }

}
