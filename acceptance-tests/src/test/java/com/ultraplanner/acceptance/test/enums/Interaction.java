package com.ultraplanner.acceptance.test.enums;

public enum Interaction {
    CLICK, CLEAR, TYPE, MOVE_TO_ELEMENT, RELEASE, CLICK_AND_HOLD, SELECT, DOUBLE_CLICK;
}
