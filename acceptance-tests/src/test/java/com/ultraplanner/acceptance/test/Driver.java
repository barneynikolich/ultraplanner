package com.ultraplanner.acceptance.test;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import com.ultraplanner.acceptance.test.enums.BrowserType;

import java.util.concurrent.TimeUnit;

public class Driver {

    private static BrowserType browserType = BrowserType.CHROME;

    private static WebDriver driver;
    private static Actions action;
    private static int waitTime = 40;
    private static Boolean setWindowSize = true;
//    private static Elements elements = new Elements();
    private static String userName;


    public synchronized static WebDriver getCurrentDriver()
    {
        if (driver == null)
        {
            try
            {
                if (Boolean.parseBoolean(System.getProperty("keepCurrentWindowSize")))
                {
                    setWindowSize = false;
                }
                switch (browserType)
                {
                    case CHROME:
                        System.setProperty(BrowserType.CHROME.getPropertyKey(), BrowserType.CHROME.getPropertyValue());
                        ChromeOptions options = new ChromeOptions();
                        options.addArguments("--test-type --start-maximized --ignore-certificate-errors --disable-infobars");
                        driver = new ChromeDriver(options);
                        break;
                    case FIREFOX:
                        System.setProperty(BrowserType.FIREFOX.getPropertyKey(), BrowserType.FIREFOX.getPropertyValue());
                        driver = new FirefoxDriver();
                        break;
                    case IE:
                        System.setProperty(BrowserType.IE.getPropertyKey(), BrowserType.IE.getPropertyValue());
                        driver = new InternetExplorerDriver();
                        break;
                }
                setMinimumWindowSize(driver);
                setTimeoutDefault();
            }
            finally
            {
                Runtime.getRuntime().addShutdownHook(
                        new Thread(new BrowserCleanup()));
            }
        }
        action = new Actions(driver);
        return driver;
    }

    private static void setMinimumWindowSize(WebDriver driver)
    {
        if (setWindowSize)
        {
            System.out.println("Default window height: " + driver.manage().window().getSize().height);
            System.out.println("Default window width: " + driver.manage().window().getSize().width);

            driver.manage().window().setSize(new Dimension(1280, 1024));

            System.out.println("Updated window height: " + driver.manage().window().getSize().height);
            System.out.println("Updated window width: " + driver.manage().window().getSize().width);
        }
    }

    public static void close()
    {
        getCurrentDriver().quit();
    }

    public static void setTimeoutMilliseconds(int amount)
    {
        setTimeout(amount, TimeUnit.MILLISECONDS);
    }

    public static void setTimeout(int amount, TimeUnit tu)
    {
        driver.manage().timeouts().implicitlyWait(amount, tu);
    }

    public static void setTimeoutDefault()
    {
        setTimeout(waitTime, TimeUnit.SECONDS);
    }

    private static class BrowserCleanup implements Runnable
    {
        public void run()
        {
            close();
        }
    }


}
