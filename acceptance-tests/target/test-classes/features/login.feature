
Feature: Hello cucumber
  As a user
  I want to go to site
  So i can enjoy it

  Scenario: User is on right page
    Given I am on the "Website Home" page on URL "http://www.google.co.uk"
    Then I should see "<string>" title

#
#@acceptance
#Feature: Login Profile
#  As a user of ultraPlanner.com
#  I want to login and be taken to the main site
#  In order to view races
#
#  Background: User navigates to Company home page
#    Given I am on the "Website Home" page on URL "http://178.62.34.171"
##    Then I should see "Log In as Employee" message
#
#
#  Scenario: Successful login
##    When I fill in "Username" with "Test"
##    And I fill in "Password" with "123"
#    And I click on the "Log In" button
#    Then I am on the "My profile" page on URL "www.mycompany.com/myprofile"
#    And I should see "Welcome to your profile" message
#    And I should see the "Log out" button
#
##  Scenario Outline: Failed login using wrong credentials
##    When I fill in "Username" with "<username>"
##    And I fill in "Password" with "<password>"
##    And I click on the "Login" button
##    And I should see "<warning>" message
##    Examples:
##      | username    | password   | warning                           |
##      | Test        | !23        | Incorrect credentials. Try again! |
##      | Tset        | 123        | Incorrect credentials. Try again! |
##      | Tset        | !23        | Incorrect credentials. Try again! |
##      | Test        |            | Please enter password.            |
##      |             | 123        | Please enter username.            |
##      |             |            | Please enter your credentials.    |