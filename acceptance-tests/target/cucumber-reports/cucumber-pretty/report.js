$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("login.feature");
formatter.feature({
  "line": 3,
  "name": "Login Profile",
  "description": "As a user of ultraPlanner.com\r\nI want to login and be taken to the main site\r\nIn order to view races",
  "id": "login-profile",
  "keyword": "Feature",
  "tags": [
    {
      "line": 2,
      "name": "@login"
    }
  ]
});
});