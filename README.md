# ultraPlanner

### Web application to organise Ultra Marathon races and send reminders of events.

## Architecture

Tier  | Technology
---      | ---
Frontend | Angular 2, npm
Backend  | Spring Boot Application, Maven
Database | SQL Database
Server   | Linux OS, Nginx

## Installation
### Frontend

* Download npm and run:
```bash
$ npm install
```
### Running Angular Application:
```bash
$ npm start
```
### Backend

* Download Maven and run:
```bash
$ mvn spring-boot:run
```
* [How to run Spring Boot Application with Maven](https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-running-your-application.html) 

