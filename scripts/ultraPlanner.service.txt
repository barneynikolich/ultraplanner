#Save this file: /etc/systemd/system



[Unit]
Description=ultra-planner-backend
After=syslog.target

[Service]
User=root
ExecStart=/usr/bin/java -jar -Dspring.profiles.active=prod /home/ultra-planner/ultraPlannerBackend-0.0.1-SNAPSHOT.jar SuccessExitStatus=143
RestartSec=10
Restart=on-failure


[Install]
WantedBy=multi-user.target
