# Useful Resources

* [REST in Spring](https://spring.io/blog/2009/03/27/rest-in-spring-3-resttemplate)
* [Web Scraping tool - Jaunt](http://jaunt-api.com/)
* [Deploy Spring boot](https://docs.spring.io/spring-boot/docs/current/reference/html/deployment-install.html)
* [Spring JDBC](https://geeks-mexico.com/2017/09/22/tutorial-spring-boot-spring-jdbc/)
* [Spring JDBC Transactional](https://www.concretepage.com/spring-boot/spring-boot-jdbc-example)
