--INSERT INTO race VALUES
--(1,'Notts',NULL,NULL,'16/01/2019',NULL,NULL,NULL,'6H',NULL,'Newark Showground Marathon',NULL,'Trail','http://www.midlandsrunningguide.com/race/newark-showground-christmas-challenge',0,0),
--(2,'Cumbria',NULL,NULL,'16/01/2019',NULL,NULL,NULL,'38m',NULL,'Tour de Helvellyn',NULL,'Trail','https://www.nav4.co.uk/',0,0),
--(3,'Kent',NULL,NULL,'16/12/2017',NULL,NULL,NULL,'6H',NULL,'Usual Suspects 1',NULL,'Mixed','http://www.saxon-shore.com/usual_suspects/',0,0),
--(4,'Essex',NULL,NULL,'17/12/2017',NULL,NULL,NULL,'50k',NULL,'Dawn to Dusk Sunlight Ultra',NULL,'Road','http://www.sikhsinthecity.org',0,0),
--(5,'Surrey',NULL,NULL,'17/12/2017',NULL,NULL,NULL,'31m',NULL,'Mince Pi Run',NULL,'Trail','https://www.runultra.co.uk/Events/Mince-Pi-Run',0,0),
--(6,'Notts',NULL,NULL,'17/12/2017',NULL,NULL,NULL,'6H',NULL,'Newark Showground Marathon 2',NULL,'Trail','http://www.midlandsrunningguide.com/race/newark-showground-christmas-challenge',0,0),
--(7,'Kent',NULL,NULL,'17/12/2017',NULL,NULL,NULL,'6H',NULL,'Usual Suspects 2',NULL,'Mixed','http://www.saxon-shore.com/usual_suspects/',0,0),
--(9,'Shropshire',NULL,NULL,'27/12/2017',NULL,NULL,NULL,'Sunset',NULL,'Sunrise to Sunset Challenge',NULL,'Mixed','http://www.codrc.co.uk/sunrise-to-sunset-challenge',0,0),
--(10,'Hampshire',NULL,NULL,'28/12/2017',NULL,NULL,NULL,'45m',NULL,'Winter Cross 45m Ultra',NULL,'Trail','http://www.secondwindrunning.co.uk/p/winter-cross-ultra',0,0),
--(11,'Hampshire',NULL,NULL,'28/12/2017',NULL,NULL,NULL,'50k',NULL,'Winter Cross 50k Ultra',NULL,'Trail','http://www.secondwindrunning.co.uk/p/winter-cross-ultra',0,0),
--(12,'Surrey',NULL,NULL,'29/12/2017',NULL,NULL,NULL,'6H',NULL,'Frozen Phoenix 2017 1',NULL,'Mixed','http://www.phoenixrunning.co.uk/?page_id=3189',0,0),
--(13,'Surrey',NULL,NULL,'30/12/2017',NULL,NULL,NULL,'6H',NULL,'Frozen Phoenix 2017 2',NULL,'Mixed','http://www.phoenixrunning.co.uk/?page_id=3189',0,0),
--(14,'Kent',NULL,NULL,'31/12/2017',NULL,NULL,NULL,'6H',NULL,'Black Fowlmead Challenge',NULL,'Mixed','http://www.saxon-shore.com/fowlmead_challenge/',0,0),
--(15,'Kent',NULL,NULL,'31/12/2017',NULL,NULL,NULL,'6H',NULL,'Fowlmead Challenge NYE',NULL,'Mixed','http://www.saxon-shore.com/fowlmead_challenge/',0,0),
--(17,'Yorkshire',NULL,NULL,'01/01/2018',NULL,NULL,NULL,'30m',NULL,'Hardmoors Ultra 30',NULL,'Trail','https://www.hardmoors110.org.uk/hardmoors-30/',0,0),
--(18,'Kent',NULL,NULL,'01/01/2018',NULL,NULL,NULL,'6H',NULL,'New Years Day Marathon',NULL,'Mixed','http://www.saxon-shore.com/New_Years_Day_Marathon/',0,0),
--(19,'Merseyside',NULL,NULL,'06/01/2018',NULL,NULL,NULL,'32m',NULL,'Sir Titus Trot',NULL,'Mixed','http://www.itsgrimupnorthrunning.co.uk/',0,0),
--(20,'London',NULL,NULL,'07/01/2018',NULL,NULL,NULL,'6H',NULL,'Another Medal On The Wall',NULL,'Road','https://www.runningmiles.co.uk/another-medal-on-the-wall',0,0),
--(21,'Hadrians Wall','£217.28','£3.15','17/06/2017',NULL,NULL,'1510','69','22','The Wall','Price rises 1 May','Hill/farmland',NULL,0,0),
--(23,'Wales',NULL,NULL,'13/01/2018',NULL,NULL,NULL,'33.5m',NULL,'Coastal Trail Series - Angelsey',NULL,'Trail','https://www.endurancelif.com/404.asp',0,0),
--(24,'Bucks',NULL,NULL,'13/01/2018',NULL,NULL,NULL,'45m',NULL,'Country to Capital',NULL,'Trail','http://www.gobeyondultra.co.uk/events/country_to_capital_2017',0,0),
--(25,'Surrey',NULL,NULL,'13/01/2018',NULL,NULL,NULL,'30m',NULL,'North Downs Way Ultra',NULL,'Mixed','http://www.runningadventures.uk/northdownsway.html',0,0),
--(26,'Derbyshire',NULL,NULL,'13/01/2018',NULL,NULL,NULL,'108m',NULL,'The Spine Race Challenger',NULL,'Trail','http://www.thespinerace.com',0,0),
--(27,'Kent',NULL,NULL,'13/01/2018',NULL,NULL,NULL,'6H',NULL,'Winter Cakeathon Challenge',NULL,'Mixed','http://www.saxon-shore.com/winter_cakeathon/',0,0);

insert into race(id, area, cost, date, miles, name, testy, website) VALUES (1, 'Yorkshire Dales', 44, '01/01/19', 60, 'The Fellsman', 1, 'http://www.fellsman.org.uk');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (2, 'Peak District', 55, '03/08/19', 54, 'Hedgehope', 1, 'http://www.fellsman.org.uk');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (3, 'Northumberland', 3, '01/01/19', 90, '3 Peaks', 1, 'http://www.fellsman.org.uk');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (4, 'Scotland', 6, '01/03/19', 31, 'Devlis Lakes 42', 1, 'http://www.fellsman.org.uk');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (5, 'Wales', 5, '01/08/19', 60, 'Three Rings of Shap', 1, 'www.fellsman.org.uk');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (6, 'Yorkshire Dales', 55, '01/11/19', 55, 'Round the Island', 1, 'www.fellsman.org.uk');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (7, 'Wiltshire', 74, '01/01/19', 37, 'Snowdonia Quarter', 1, 'www.fellsman.org.uk');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (8, 'Scotland', 26, '04/07/19', 16, 'Western Way 50', 1, 'www.fellsman.org.uk');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (9, 'Lake District', 74, '01/03/19', 35, 'Highland Hike Challenge 100k', 1, 'www.fellsman.org.uk');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (10, 'Cheshire', 74, '01/03/19', 35, 'Highland Hike Challenge 100k', 1, 'www.fellsman.org.uk');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (11,'Hadrians Wall', 217.28,'17/03/2017','69', 'The Wall',1,'w');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (12, 'Bullock Smithy Hike', 217.28,'17/03/2017','53', 'Bullock Smithy Hike',1,'w');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (13,'Spire Ultra', 217.28,'17/04/2017','69', 'Spire Ultra',1,'w');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (14,'Three Towers Ultra', 217.28,'17/04/2017','31', 'Three Towers Ultra',1,'w');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (15,'Pennine 39', 217.28,'18/03/2017','69', 'Pennine 39',1,'w');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (16,'Calderdale Hike', 217.28,'19/04/2017','23', 'Calderdale Hike',1,'w');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (17,'Lakes Mountain 42', 217.28,'20/03/2017','77', 'Lakes Mountain 42',1,'w');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (18,'Haworth Hobble', 217.28,'17/05/2017','83', 'Haworth Hobble',1,'w');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (19,'Kent', 100.28,'17/05/2018','21', 'Fowlmead Challenge',1,'w');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (20,'Yorkshire Dales', 60.28,'12/06/2019','64', 'Hardmoors Ultra 30',1,'w');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (21,'Wales', 20.28,'05/08/2019','53', 'New Years Day Marathon',1,'w');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (22,'Derbyshire', 27.28,'20/09/2019','23', 'Sir Titus Trot',1,'w');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (23,'Wales', 7.28,'29/11/2019','33', 'Coastal Trail Series - Angelsey',1,'w');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (24,'Surrey', 7.28,'29/11/2019','33', 'North Downs Way Ultra',1,'w');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (25,'Derbyshire', 7.28,'29/11/2019','33', 'Coastal Trail Series - Angelsey',1,'w');
insert into race(id, area, cost, date, miles, name, testy, website) VALUES (26,'Surrey', 7.28,'29/11/2019','268', 'The Spine Race Challenger',1,'w');


--(17,'Yorkshire',NULL,NULL,'01/01/2018',NULL,NULL,NULL,'30m',NULL,'Hardmoors Ultra 30',NULL,'Trail','https://www.hardmoors110.org.uk/hardmoors-30/',0,0),
--(18,'Kent',NULL,NULL,'01/01/2018',NULL,NULL,NULL,'6H',NULL,'New Years Day Marathon',NULL,'Mixed','http://www.saxon-shore.com/New_Years_Day_Marathon/',0,0),
--(19,'Merseyside',NULL,NULL,'06/01/2018',NULL,NULL,NULL,'32m',NULL,'Sir Titus Trot',NULL,'Mixed','http://www.itsgrimupnorthrunning.co.uk/',0,0),
--(20,'London',NULL,NULL,'07/01/2018',NULL,NULL,NULL,'6H',NULL,'Another Medal On The Wall',NULL,'Road','https://www.runningmiles.co.uk/another-medal-on-the-wall',0,0),
--(21,'Hadrians Wall','£217.28','£3.15','17/06/2017',NULL,NULL,'1510','69','22','The Wall','Price rises 1 May','Hill/farmland',NULL,0,0),
--(23,'Wales',NULL,NULL,'13/01/2018',NULL,NULL,NULL,'33.5m',NULL,'Coastal Trail Series - Angelsey',NULL,'Trail','https://www.endurancelif.com/404.asp',0,0),
--(24,'Bucks',NULL,NULL,'13/01/2018',NULL,NULL,NULL,'45m',NULL,'Country to Capital',NULL,'Trail','http://www.gobeyondultra.co.uk/events/country_to_capital_2017',0,0),
--(25,'Surrey',NULL,NULL,'13/01/2018',NULL,NULL,NULL,'30m',NULL,'North Downs Way Ultra',NULL,'Mixed','http://www.runningadventures.uk/northdownsway.html',0,0),
--(26,'Derbyshire',NULL,NULL,'13/01/2018',NULL,NULL,NULL,'108m',NULL,'The Spine Race Challenger',NULL,'Trail','http://www.thespinerace.com',0,0),
--(27,'Kent',NULL,NULL,'13/01/2018',NULL,NULL,NULL,'6H',NULL,'Winter Cakeathon Challenge',NULL,'Mixed','http://www.saxon-shore.com/winter_cakeathon/',0,0);
