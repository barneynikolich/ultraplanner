package com.example.ultraPlannerBackend.controllers;

import com.example.ultraPlannerBackend.services.RaceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Barn on 22/12/2017.
*/
@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/locations")
public class LocationController {

    @GetMapping
    public List<String> findAll() {
        return getLocations();
    }

    public List<String> getLocations() {

        List<String> locations = new ArrayList<>();
        locations.add("Peak District");
        locations.add("Surrey");
        locations.add("Derbyshire");
        locations.add("Yorkshire Dales");
        locations.add("Devon");
        locations.add("Lake District");
        locations.add("Northumberland");
        locations.add("Cheshire");
        locations.add("Ireland");
        locations.add("Cotswolds");
        locations.add("Oxford");
        locations.add("Cumbria");

        return locations;
    }
}
