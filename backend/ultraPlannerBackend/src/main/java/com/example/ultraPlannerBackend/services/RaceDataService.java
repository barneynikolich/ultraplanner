package com.example.ultraPlannerBackend.services;

import com.example.ultraPlannerBackend.entities.Race;

import java.util.List;

public interface RaceDataService {

    Race save(Race race);
    void deleteById(Long id);
    void edit(Race race);
    Race findRace(Long id);
    List<Race> getUpcomingRaces();
    List<Race> findAll(String terrain, String distance, String area, String dateSort);

//    ApexRace saveApexRace(ApexRace race);

}
