package com.example.ultraPlannerBackend.controllers;

import com.example.ultraPlannerBackend.entities.Race;
import com.example.ultraPlannerBackend.services.RaceDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Barn on 20/12/2017.
 */

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/terrains")
public class TerrainController {

    @Autowired
    RaceDataService raceDataService;

    @GetMapping
    public List<String> findAll() {
        return getTerrains();
    }

    // TODO: Get Terrain information from TerrainService (populate list of terrains from current terrains in database)
    public List<String> getTerrains() {

        List<String> terrains = new ArrayList<>();
        terrains.add("Beach");
        terrains.add("Farmland");
        terrains.add("Coastal");
        terrains.add("Canal towpath");
        terrains.add("Hill");
        terrains.add("Trail");
        terrains.add("Tracks");
        terrains.add("Moorland");
        terrains.add("Woodland");
        terrains.add("Old Railway");
        terrains.add("Estuary");
        terrains.add("Mountain");
        terrains.add("Urban");
        terrains.add("Forest");
        terrains.add("River");
        terrains.add("Valley");
        terrains.add("Lochside");
        terrains.add("Mixed");

        return terrains;
    }

}
