package com.example.ultraPlannerBackend.services;

import com.example.ultraPlannerBackend.controllers.domain.ContactRequest;
import com.example.ultraPlannerBackend.repositories.ContactRequestRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Barn on 14/08/2018.
 */
@Service
public class ContactRequestService {

    private final Logger logger = LoggerFactory.getLogger(ContactRequestService.class);

    @Autowired
    MailGunService mailGunService;

    @Autowired
    ContactRequestRepository contactRequestRepository;

    public ContactRequest saveRequest(ContactRequest contactRequest)
    {
        logger.info("Sending email to " + contactRequest.getFullName());
        this.mailGunService.sendEmail(contactRequest);
        return this.contactRequestRepository.save(contactRequest);
    }


}
