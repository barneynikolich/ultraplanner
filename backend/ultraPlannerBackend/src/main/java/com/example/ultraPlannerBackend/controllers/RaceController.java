package com.example.ultraPlannerBackend.controllers;

import com.example.ultraPlannerBackend.entities.Race;
import com.example.ultraPlannerBackend.services.JsonConverterService;
import com.example.ultraPlannerBackend.utils.logging.LoggingHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.example.ultraPlannerBackend.services.RaceDataService;

import java.util.List;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/races")
public class RaceController {

    private final Logger logger = LoggerFactory.getLogger(RaceController.class);

    @Autowired
    RaceDataService raceDataService;

    @GetMapping
    public List<Race> findAllRaces(@RequestParam("terrain") Optional<String> terrain,
                                   @RequestParam("distance") Optional<String> distance,
                                   @RequestParam("area") Optional<String> area,
                                   @RequestParam("dateSort") Optional<String> dateSort) {

        return raceDataService.findAll(terrain.orElse(null), distance.orElse(null), area.orElse(null), dateSort.orElse(null));
    }

    @GetMapping(value = "/upcoming")
    public List<Race> getUpcomingRaces() {
        return raceDataService.getUpcomingRaces();
    }

    @GetMapping(value = "/{id}")
    public Race findRaceById(@PathVariable Long id) {
        return raceDataService.findRace(id);
    }

    @PostMapping
    public @ResponseBody Race saveRace(@RequestBody Race race) {
        logger.info(String.format("Saving: %s", race));
        return raceDataService.save(race);
    }

    @DeleteMapping(value = "/{id}")
    public void deleteRace(@PathVariable Long id) {
        raceDataService.deleteById(id);
    }

    @PutMapping
    public void editRace(@RequestBody Race race) {
        raceDataService.edit(race);
    }


}

