/**
 * Copyright © 2017 Lhasa Limited
 * File created: 2 Mar 2017 by Mohammed Sultan
 * Creator : Mohammed Sultan
 * Version : $Id$
 */
package com.example.ultraPlannerBackend.controllers;

import java.time.Instant;

import org.springframework.http.HttpStatus;
import org.springframework.web.context.request.WebRequest;

/**
 * @author Mohammed Sultan
 * @since 2 Mar 2017
 */
public class ServerFailureResponse
{
    
    	public String generate(
			WebRequest request, String errorMessage, String exceptionType, HttpStatus httpStatus)
	{              
		JsonErrorHttpBody json = new JsonErrorHttpBody(
				currentDateTime(),
				httpStatus,
				httpStatus.name().replace("_"," "),
				exceptionType,
				errorMessage,
				getUri(request));
		return json.toJsonString();
	}
        
            	public String generate(
			String requestUri, String errorMessage, String exceptionType, HttpStatus httpStatus)
	{                
		JsonErrorHttpBody json = new JsonErrorHttpBody(
				currentDateTime(),
				httpStatus,
				httpStatus.name().replace("_"," "),
				exceptionType,
				errorMessage,
				requestUri);
		return json.toJsonString();
	}
        
	private String currentDateTime()
	{
		return Long.toString(Instant.now().getEpochSecond());
	}

	private String getUri(WebRequest request)
	{
		return request.getDescription(false).replace("uri=", "");
	}
}
/* ---------------------------------------------------------------------*
 * This software is the confidential and proprietary
 * information of Lhasa Limited
 * Granary Wharf House, 2 Canal Wharf, Leeds, LS11 5PS
 * ---
 * No part of this confidential information shall be disclosed
 * and it shall be used only in accordance with the terms of a
 * written license agreement entered into by holder of the information
 * with LHASA Ltd.
 * --------------------------------------------------------------------- */
