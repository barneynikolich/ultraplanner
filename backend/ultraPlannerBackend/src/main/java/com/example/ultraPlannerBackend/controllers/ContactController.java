package com.example.ultraPlannerBackend.controllers;

import com.example.ultraPlannerBackend.controllers.domain.ContactRequest;
import com.example.ultraPlannerBackend.services.ContactRequestService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Barn on 14/08/2018.
 */
@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1/contactRequest")
public class ContactController {

    private final Logger logger = LoggerFactory.getLogger(ContactController.class);

    @Autowired
    ContactRequestService contactRequestService;

    @PostMapping
    public void saveContactRequest(@RequestBody ContactRequest contactRequest) {
        logger.info(String.format("Contact request from: %s", contactRequest));
        this.contactRequestService.saveRequest(contactRequest);
    }

}
