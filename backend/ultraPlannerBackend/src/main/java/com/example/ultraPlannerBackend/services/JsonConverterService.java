package com.example.ultraPlannerBackend.services;

import com.example.ultraPlannerBackend.UltraPlannerBackendApplication;
import com.example.ultraPlannerBackend.entities.Race;
import com.example.ultraPlannerBackend.entities.RaceBean;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;


@Service
public class JsonConverterService {

    Gson gson;

    public JsonConverterService() {
        this.gson = new Gson();
    }

    public List<Race> getListOfRacesFromJsonFile() {
        return gson.fromJson(loadRacesJsonFromResources(), new TypeToken<List<Race>>() {
        }.getType());
    }

    public List<RaceBean> getListOfRaceBeansFromJsonFile() {
        return gson.fromJson(loadRacesJsonFromResources(), new TypeToken<List<RaceBean>>() {
        }.getType());
    }

    public String getJsonValueFromRace(Race race) {
        return gson.toJson(race);
    }

    public String getJsonValueFromRaceList(List<Race> races) {
        return gson.toJson(races);
    }

    private String loadRacesJsonFromResources() {
        StringBuilder result = new StringBuilder("");

        try {
            InputStream inputstream = UltraPlannerBackendApplication.class.getResourceAsStream("/racesJson.json");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputstream));
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                result.append(line);
            }

            inputstream.close();
        } catch (IOException err) {
            err.printStackTrace();
        }

        return result.toString();
    }

}
