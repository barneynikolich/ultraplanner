package com.example.ultraPlannerBackend.services;

import com.example.ultraPlannerBackend.entities.Race;
import com.example.ultraPlannerBackend.repositories.FinalRaceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FinalRaceService {

    @Autowired
    FinalRaceRepository raceRepository;

    public List<Race> findAll() {
        return raceRepository.findAll();
    }

    public Race findRace(Long id) {
        return raceRepository.findOne(id);
    }

    public Race saveRace(Race race) {
        return raceRepository.save(race);
    }

    public void deleteById(Long id) {
        raceRepository.delete(id);
    }

}
