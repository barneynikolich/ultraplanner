//package com.example.ultraPlannerBackend.controllers;
//
//import com.example.ultraPlannerBackend.entities.ApexRace;
//import com.example.ultraPlannerBackend.entities.Race;
//import com.example.ultraPlannerBackend.entities.RaceBean;
//import com.example.ultraPlannerBackend.repositories.ApexRaceRepository;
//import com.example.ultraPlannerBackend.repositories.RaceRepository;
//import com.example.ultraPlannerBackend.services.FinalRaceService;
//import com.example.ultraPlannerBackend.services.HtmlScrapeService;
//import com.example.ultraPlannerBackend.services.JsonConverterService;
//import com.example.ultraPlannerBackend.services.RaceDataService;
//import jdk.nashorn.internal.runtime.options.Option;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.repository.query.Param;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Optional;
//import java.util.Scanner;
//
//@CrossOrigin
//@RestController
//@RequestMapping(value = "/api/v1/data")
//public class RaceDataController {
//
//    @Autowired
//    FinalRaceService raceDataService;
//
//    @Autowired
//    ApexRaceRepository apexRaceRepository;
//
//    @Autowired
//    RaceRepository raceRepository;
//
//    @Autowired
//    JsonConverterService jsonConverterService;
//
//    @GetMapping
//    public List<Race> findAllRaces() {
//        return raceDataService.findAll();
//    }
//
//    @GetMapping(value = "/{id}")
//    public Race findRaceById(@PathVariable Long id) {
//        return raceDataService.findRace(id);
//    }
//
//    @PostMapping
//    public @ResponseBody Race saveRace(@RequestBody Race race) {
//        return raceDataService.saveRace(race);
//    }
//
//    @DeleteMapping(value = "/{id}")
//    public void deleteRace(@PathVariable Long id) {
//        raceDataService.deleteById(id);
//    }
//
//    public Integer count = 0;
//    List<Race> finalRaces = new ArrayList<>();
//
//    @GetMapping(value = "process")
//    public void processData() {
//
//        Scanner sc = new Scanner(System.in);
//
////        List<RaceBean> runRaces = raceRepository.findAll();
//        List<ApexRace> apexRaces = apexRaceRepository.findAll();
//
//        for (ApexRace apex : apexRaces) {
//
//            for (RaceBean race : runRaces) {
//
//                if (apex.getName().contains(race.getName())) {
//                    System.out.println("[Match] \nAPEX:    " + apex.getName() + "\nRUNF:    " + race.getName() + "\n");
//                    System.out.println("Choose an option: a, r, or b");
//
//                    while (sc.hasNext()) {
//                        String s1 = sc.nextLine();
//                        if (s1.equals("b")) {
//                            finalRaces.add(convertApexToRace(apex));
//                            finalRaces.add(convertRaceBeanToRace(race, Optional.empty(), Optional.empty()));
//                            System.out.println("Both selected");
//                            System.out.println(finalRaces.size());
//                            break;
//                        } else if (s1.equals("a")) {
//                            finalRaces.add(convertApexToRace(apex));
//                            System.out.println(finalRaces.size());
//                            System.out.println("apex selected");
//                            break;
//                        } else if (s1.equals("r")) {
//                            finalRaces.add(convertRaceBeanToRace(race, Optional.empty(), Optional.empty()));
//                            System.out.println(finalRaces.size());
//                            System.out.println("apex selected");
//                            break;
//                        }
//                        else if (s1.equals("rw")) {
//                            finalRaces.add(convertRaceBeanToRace(race, Optional.of(apex.getWebsite()), Optional.of(apex.getDate())));
//                            System.out.println(finalRaces.size());
//                            break;
//                        }
//                        else if (s1.equals("n")) {
//                            break;
//                        } else {
//                        }
//                    }
//
//                } else {
//                }
//            }
//
//            if (finalRaces.contains(apex)) {
//                System.out.println("Cannot add apex already exists");
//            } else {
//                System.out.println("Adding apex because it didnt exist");
//                finalRaces.add(convertApexToRace(apex));
//                System.out.println(finalRaces.size());
//            }
//        }
//
//        finalRaces.forEach((race) -> {
//            raceDataService.saveRace(race);
//        });
//
//    }
//
//    private boolean isRaceData(String rowData){
//        if(rowData.matches("(January.*)|(February.*)|(March.*)|(April.*)|(May.*)|(June.*)|(July.*)|(August.*)|(September.*)|(October.*)|(November.*)|(December.*)|")) {
//            return false;
//        } else {
//            return true;
//        }
//    }
//
//    private Race convertRaceBeanToRace(RaceBean race, Optional<String> website, Optional<String> date) {
//        Race r = new Race();
//        r.setId(finalRaces.size());
//        r.setName(race.getName());
//        r.setCostMile(race.getCostMile());
//        r.setArea(race.getArea());
//        r.setCost(race.getCost());
//        if (date.isPresent()) {
//            r.setDate(date.get());
//        } else {
//            r.setDate(race.getDate());
//        }
//        r.setMiles(race.getMiles());
//        r.setKm(race.getKm());
//        r.setMclimb(race.getMclimb());
//        r.setMkm(race.getMkm());
//        r.setNotes(race.getNotes());
//        r.setTerrain(race.getTerrain());
//        if (website.isPresent()) {
//            r.setWebsite(website.get());
//        }
//        System.out.println("Adding " + r.getName());
//        return r;
//    }
//
//    private Race convertApexToRace(ApexRace race) {
//        Race r = new Race();
//        r.setId(finalRaces.size());
//        r.setName(race.getName());
//        r.setArea(race.getArea());
//        r.setDate(race.getDate());
//        r.setMiles(race.getMiles());
//        r.setTerrain(race.getTerrain());
//        r.setWebsite(race.getWebsite());
//        System.out.println("Adding " + r.getName());
//        return r;
//
//    }
//}
//
