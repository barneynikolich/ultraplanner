package com.example.ultraPlannerBackend.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.transaction.Transactional;

@Entity
@Transactional
public class RaceBean {

    @Id
    @GeneratedValue
    private long id;

    private String date;
    private String name;
    private String miles;
    private String km;
    private String mclimb;
    private String mkm;
    private String area;
    private String eventType;
    private String terrain;
    private String cost;
    private String costMile;
    private String notes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiles() {
        return miles;
    }

    public void setMiles(String miles) {
        this.miles = miles;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getMclimb() {
        return mclimb;
    }

    public void setMclimb(String mclimb) {
        this.mclimb = mclimb;
    }

    public String getMkm() {
        return mkm;
    }

    public void setMkm(String mkm) {
        this.mkm = mkm;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getTerrain() {
        return terrain;
    }

    public void setTerrain(String terrain) {
        this.terrain = terrain;
    }

    public String getCost() {
        return cost;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getCostMile() {
        return costMile;
    }

    public void setCostMile(String costMile) {
        this.costMile = costMile;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
