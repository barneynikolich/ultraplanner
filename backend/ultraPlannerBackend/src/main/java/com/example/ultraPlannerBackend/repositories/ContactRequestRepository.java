package com.example.ultraPlannerBackend.repositories;

import com.example.ultraPlannerBackend.controllers.domain.ContactRequest;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Barn on 14/08/2018.
 */
public interface ContactRequestRepository extends JpaRepository<ContactRequest, Long> {
}
