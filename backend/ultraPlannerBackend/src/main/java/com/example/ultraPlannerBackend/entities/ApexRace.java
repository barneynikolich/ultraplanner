package com.example.ultraPlannerBackend.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by Barn on 26/12/2017.
 */
@Entity
public class ApexRace {

    @Id
    @GeneratedValue
    private long id;

    private String date;
    private String name;
    private String area;
    private String miles;
    private String terrain;
    private String website;

    public ApexRace(){}

    public ApexRace(String[] raceData) {

        this.date = raceData[0];
        this.name = raceData[1];
        this.area = raceData[2];
        this.miles = raceData[3];
        this.terrain = raceData[4];
        this.website = raceData[5];
    }

    @Override
    public String toString() {
        return String.format( "%-12s %-40s %-15s %-10s %-10s %-20s", getDate(), getName(), getArea(), getMiles(), getTerrain(), getWebsite());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getMiles() {
        return miles;
    }

    public void setMiles(String miles) {
        this.miles = miles;
    }

    public String getTerrain() {
        return terrain;
    }

    public void setTerrain(String terrain) {
        this.terrain = terrain;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
