package com.example.ultraPlannerBackend.utils;

import com.example.ultraPlannerBackend.entities.ApexRace;
import com.example.ultraPlannerBackend.entities.Race;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

@Service
public class HtmlScrapeService {

    private final String FILENAME = "races.html";
    private final String APEXRACES = "apexRaces.html";
//    private final Integer HEADER_LENGTH = getTableHeaders().size();
    private final String RUNFURTHER_URL = "http://runfurther.com/ultra-calendar/";
    private final String APEX_SPORTS_URL = "http://www.apex-sports.co.uk/document/item-65-2017-18-uk-ultra-marathon-listing";

    public HtmlScrapeService() {}
    public ArrayList<Race> getRaces() {
        return convertToRaceScrape(getRows());
    }

    public ArrayList<ApexRace> getApexRace() {
        return convertToApexRace(getRows());
    }

    public  ArrayList<ApexRace> convertToApexRace(ArrayList<ArrayList<String>> allRows) {
        ArrayList<ApexRace> races = new ArrayList<>();

        for(int row = 0; row < allRows.size(); row ++) {
            String[] arr = new String[7];

            if(allRows.get(row).size() != 0) { // Do not add empty rows
                for(int data = 0; data < allRows.get(row).size(); data++) {
                    String currentRowData = allRows.get(row).get(data);
                    arr[data] = currentRowData;
                }
                ApexRace r = new ApexRace(arr);
                races.add(r);
            }
        }
        return races;
    }

    public  ArrayList<Race> convertToRaceScrape(ArrayList<ArrayList<String>> allRows) {
        ArrayList<Race> races = new ArrayList<>();

        for(int row = 0; row < allRows.size(); row ++) {
            String[] arr = new String[10];

            if(allRows.get(row).size() != 0) { // Do not add empty rows
                for(int data = 0; data < allRows.get(row).size(); data++) {
                    String currentRowData = allRows.get(row).get(data);
                    arr[data] = currentRowData;
                }

                Race r = new Race(arr);
                races.add(r);
            }
        }
        return races;
    }

    public ArrayList<String> getTableHeaders() {
        ArrayList<String> tableHeaders = new ArrayList<>();

        Document localDoc = parseHTMLFile(APEXRACES);
//        Element table = getHtmlElement("tablepress-3", localDoc);
        Element table = localDoc.select("table").get(0);
        Elements childElements = getChildElements(table, "thead tr td");

        for (Element element: childElements) {
            if (element.html().equals("Laps")) {

            }
        }
        return populateList(tableHeaders, childElements);
    }

    public ArrayList<ArrayList<String>> getRows() {
        ArrayList<ArrayList<String>> races = new ArrayList<>();
        Document localDoc = parseHTMLFile(APEXRACES);

        Element table = localDoc.select("table").get(0); //select the first table.
//        Element table = getHtmlElement(tableElement, localDoc);
        Elements tableRowElements = getChildElements(table, ":not(thead) tr");


        for (int i = 0; i < tableRowElements.size(); i++) {
            Element row = tableRowElements.get(i);
            Elements rowItems = row.select("td");

            ArrayList<String> rowData  = new ArrayList<>();

            for(int j = 0; j < rowItems.size(); j++){

                if (j == rowItems.size() -1 && !rowItems.get(j).html().equals("Website")) {
                    rowItems.get(j).select("a").attr("href");
                    rowData.add(rowItems.get(j).select("a").attr("href"));
                } else {
                    String tableData = rowItems.get(j).text();
                    if(!tableData.isEmpty() && isRaceData(tableData) && isNotLap(tableData)) {
                        rowData.add(rowItems.get(j).text());
                    } else if (!isRaceData(tableData)) {
                        rowData.add(rowItems.get(j).text());
                    }
                }
            }
            races.add(rowData);
        }
        return races;
    }

    private ArrayList<String> populateList(ArrayList<String> list, Elements data) {
        for (int i = 0; i < data.size(); i++) {
            if (!data.get(i).text().equals("Laps")) {
                list.add(data.get(i).text());
            }
        }
        return list;
    }

    private boolean isRaceData(String rowData){
        if(rowData.matches("(January.*)|(February.*)|(March.*)|(April.*)|(May.*)|(June.*)|(July.*)|(August.*)|(September.*)|(October.*)|(November.*)|(December.*)|")) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isNotLap(String rowData) {
        if (rowData.matches("Yes||No||Laps")) {
            return false;
        } else {
            return true;
        }
    }

    private Element getHtmlElement(String element, Document htmlDoc) {
        return htmlDoc.getElementById(element);
    }

    private Elements getChildElements(Element element, String cssQuery) {
        return element.select(cssQuery);
    }

    private Document parseHTMLFile(String filename) {
        Document doc;
        try {
            File input = getFileFromResources(filename);
            doc = Jsoup.parse(input, "UTF-8");
        } catch (IOException err) {
            err.printStackTrace();
            doc = null;
        }
        return doc;
    }

    private Document parseHTMLUrl(String url) {
        Document doc;
        try {
            doc = Jsoup.connect(url).get();
        } catch (IOException err) {
            err.printStackTrace();
            doc = null;
        }
        return doc;
    }

    private File getFileFromResources(String filename){
        ClassLoader classLoader = getClass().getClassLoader();
        return new File(classLoader.getResource(filename).getFile());
    }
}
