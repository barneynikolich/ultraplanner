package com.example.ultraPlannerBackend.controllers;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;

import com.google.gson.Gson;

public class JsonErrorHttpBody
{
	Gson gson = new Gson();
	private String timestamp;
	private String status;
	private String error;
	private String exception;
	private String message;
	private String path;

	public JsonErrorHttpBody(String timestamp, HttpStatus status, String error, String exception, String message, String path)
	{
		this.timestamp = timestamp;
		this.status = status.toString();
		this.error = error;
		this.exception = exception;
		this.message = message;
		this.path = path;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public String getStatus() {
		return status;
	}

	public String getError() {
		return error;
	}

	public String getException() {
		return exception;
	}

	public String getMessage() {
		return message;
	}

	public String getPath() {
		return path;
	}

	public String toJsonString()
	{
		return new JSONObject()
				.put("timestamp", timestamp)
				.put("status", status)
				.put("error", error)
				.put("exception", exception)
				.put("message", message)
				.put("path", path)
				.toString();
	}
}