package com.example.ultraPlannerBackend.entities;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Blob;

/**
 * Created by Barn on 08/11/2017.
 */
@Entity
@Table(name = "race")
public class Race {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long id;

    private String date;
    private String name;
    private String miles;
    private String km;
    private String mclimb;
    private String mkm;
    private String area;
    private String eventType;
    private String terrain;
    private String cost;
    private String costMile;
    private String notes;
    private String website;

    @Transient
    private String encodedImage;

    private int testy;

    @Lob
    @Column()
    private byte[] image;

    public Race() {
    }

    public Race(String[] raceData) {

        this.date = raceData[0];
        this.name = raceData[1];
        this.miles = raceData[2];
        this.km = raceData[3];
        this.mclimb = raceData[4];
        this.mkm = raceData[5];
        this.area = raceData[6];
        this.eventType = raceData[7];
        this.terrain = raceData[8];
        this.cost = raceData[9];
        this.costMile = raceData[10];
        this.notes = raceData[11];
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }


    public void tableHeader() {
        System.out.println(String.format("%s", "----------------------------------------------------------------------------------------------------------------"));
        System.out.println(String.format("%-13s %-40s %-15s %-10s %-10s %-20s", "| Date", "Name", "Area", "Miles", "Terrain", "Website            |"));
        System.out.println(String.format("%s", "|---------------------------------------------------------------------------------------------------------------|"));

    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public String getName() {
        return name;
    }

    public String getMiles() {
        return miles;
    }

    public String getKm() {
        return km;
    }

    public String getMclimb() {
        return mclimb;
    }

    public String getMkm() {
        return mkm;
    }

    public String getArea() {
        return area;
    }

    public String getEventType() {
        return eventType;
    }

    public String getTerrain() {
        return terrain;
    }

    public String getCost() {
        return cost;
    }

    public String getCostMile() {
        return costMile;
    }

    public String getNotes() {
        return notes;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMiles(String miles) {
        this.miles = miles;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public void setMclimb(String mclimb) {
        this.mclimb = mclimb;
    }

    public void setMkm(String mkm) {
        this.mkm = mkm;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public void setTerrain(String terrain) {
        this.terrain = terrain;
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public void setCostMile(String costMile) {
        this.costMile = costMile;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getEncodedImage() {
        return encodedImage;
    }

    public void setEncodedImage(String encodedImage) {
        this.encodedImage = encodedImage;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getTesty() {
        return testy;
    }

    public void setTesty(int testy) {
        this.testy = testy;
    }

}
