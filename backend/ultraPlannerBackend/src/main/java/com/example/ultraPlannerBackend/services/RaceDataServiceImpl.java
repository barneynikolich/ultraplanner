package com.example.ultraPlannerBackend.services;

import com.example.ultraPlannerBackend.entities.Race;
import com.example.ultraPlannerBackend.repositories.RaceRepository;
import com.example.ultraPlannerBackend.utils.DateSorter;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.io.UnsupportedEncodingException;
import java.util.*;

@Service
public class RaceDataServiceImpl implements RaceDataService {

    @Autowired
    RaceRepository raceRepository;

    @Autowired
    EntityManager em;

    @Override
    public List<Race> findAll(String terrain, String distance, String area, String dateSort) {
        CriteriaBuilder qb = em.getCriteriaBuilder();
        CriteriaQuery cq = qb.createQuery();
        Root<Race> race = cq.from(Race.class);
        List<Predicate> predicates = new ArrayList<>();

        if (filtersApplied(terrain, distance, area)) {
            buildQueryPredicates(predicates, terrain, distance, area, race, qb);
        }

        cq.select(race)
                .where(predicates.toArray(new Predicate[]{}));

        List<Race> races = em.createQuery(cq).getResultList();

        convertImageBytesToString(races);

        if (dateSort != null) {
            races = sortRacesByDate(races, dateSort);
        }

        return convertDateReadableDate(races);
    }

    private void buildQueryPredicates(List<Predicate> predicates, String terrain, String distance, String area, Root<Race> race, CriteriaBuilder qb) {
        buildPredicate(predicates, terrain, "terrain", qb, race);
        buildPredicate(predicates, area, "area", qb, race);

        if (distance != null) {
            String [] distances = getDistanceArray(distance);
            if (distances[0].equals("100")) {
                predicates.add(
                        qb.greaterThan(race.get("miles"), 100));
            } else {
                predicates.add(
                        qb.between(race.get("miles"), distances[0], distances[1]));
            }
        }
    }

    private void buildPredicate(List<Predicate> predicates, String predicate, String column, CriteriaBuilder qb, Root<Race> race)
    {
        if (predicate != null)
        {
            predicates.add(qb.equal(race.get(column), predicate));
        }
    }

    private boolean filtersApplied(String terrain, String distance, String area) {
        return terrain != null || distance != null || area != null;
    }

    private void convertImageBytesToString(List<Race> races) {
        races.forEach((Race race) -> {
            if (race.getImage() != null) {
                System.out.println("RACE HAS IMAGE:" + race.getName());
                System.out.println(race.getImage());

                String encodedImage = null;
                try {
                    byte[] bytes = race.getImage();
                    encodedImage = new String(bytes, "UTF-8"); //
                } catch (UnsupportedEncodingException e) {
                    System.err.println(e);
                }

//                String encodedImage = Base64.getEncoder().encodeToString(race.getImage());
                race.setEncodedImage(encodedImage);
                System.out.println("ENCODED IMAGE:" + encodedImage);
            }
        });
    }

    @Override
    public List<Race> getUpcomingRaces() {
        List<Race> races = raceRepository.findAll();
        List<Race> newRaces = new ArrayList<>();

        DateTimeFormatter dateformat = DateTimeFormat.forPattern("dd/MM/yyyy");
        races.forEach((Race race) -> {
            int month = dateformat.parseDateTime(race.getDate()).getMonthOfYear();
            if (month == DateTime.now().getMonthOfYear()) {
                newRaces.add(race);
            }
        });

        Collections.sort(newRaces, new DateSorter("asc"));
        return  convertDateReadableDate(newRaces);
    }

    @Override
    public Race findRace(Long id) {
        return raceRepository.findByid(id);
    }

    @Override
    public Race save(Race race) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter fmtNew = DateTimeFormat.forPattern("dd/MM/yyyy");
        if (race.getDate() != null) {
            DateTime jodaTime = fmt.parseDateTime(race.getDate());
            race.setDate(fmtNew.print(jodaTime));
        }
        return this.raceRepository.save(race);
    }

    @Override
    public void deleteById(Long id) {
        raceRepository.delete(id);
    }

    @Override
    public void edit(Race raceToEdit) {
        Race race = raceRepository.findByid(raceToEdit.getId());
        race.setName(raceToEdit.getName());
        race.setDate(raceToEdit.getDate());
        race.setMiles(raceToEdit.getMiles());
        race.setKm(raceToEdit.getKm());
        race.setArea(raceToEdit.getArea());

        raceRepository.saveAndFlush(race);
    }

    private String[] getDistanceArray(String distance) {
        ArrayList<String> tokensList;
        String [] tokens;

        if (distance.contains("-")) {
            tokens = distance.split("-");
            return tokens;
        } else if (distance.contains(">")) {
            tokens = distance.split(">");
            tokensList = new ArrayList<>(Arrays.asList(tokens));
            tokensList.removeIf(e -> e.isEmpty());
            return tokensList.toArray(tokens);
        }
        return null;
    }

    private List<Race> convertDateReadableDate(List<Race> races) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern("dd/MM/yyyy");
        DateTimeFormatter fmtNew = DateTimeFormat.forPattern("EEE dd MMM");

        races.forEach((race) -> {
            DateTime jodaTime = fmt.parseDateTime(race.getDate());
            race.setDate(fmtNew.print(jodaTime));
        });
        return races;
    }

    public List<Race> sortRacesByDate(List<Race> races, String sort) {
        Collections.sort(races, new DateSorter(sort));
        return races;
    }
}
