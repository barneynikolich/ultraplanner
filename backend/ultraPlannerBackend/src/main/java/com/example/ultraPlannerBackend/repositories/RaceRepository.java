package com.example.ultraPlannerBackend.repositories;

import com.example.ultraPlannerBackend.entities.Race;
import com.example.ultraPlannerBackend.entities.RaceBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RaceRepository extends JpaRepository<Race, Long> {

    List<Race> findByTerrainContainingAndAreaContaining(String terrain, String area);

    List<Race> findByAreaContaining(String area);

    List<Race> findByTerrainContaining(String terrain);

    Race findByid(Long id);

    List<Race> findByAreaContainingAndMilesBetween(String area, String min, String max);

    List<Race> findByTerrainContainingAndMilesBetween(String terrain, String min, String max);

    List<Race> findByTerrainContainingAndAreaContainingAndMilesBetween(String terrain, String area, String min, String max);

    @Query("SELECT r FROM Race r WHERE r.miles BETWEEN :min AND :max")
    List<Race> findMilesBetween(@Param("min") String min, @Param("max") String max);

    @Query("SELECT t FROM Race t where t.miles > 100")
    List<Race> findMilesGreaterThan();

}