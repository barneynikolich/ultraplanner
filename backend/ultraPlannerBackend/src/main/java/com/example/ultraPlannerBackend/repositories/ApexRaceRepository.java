package com.example.ultraPlannerBackend.repositories;

import com.example.ultraPlannerBackend.entities.ApexRace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Barn on 26/12/2017.
 */
@Repository
public interface ApexRaceRepository extends JpaRepository<ApexRace, Long> {
}
