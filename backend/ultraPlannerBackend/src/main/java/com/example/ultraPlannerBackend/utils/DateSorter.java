package com.example.ultraPlannerBackend.utils;

import com.example.ultraPlannerBackend.entities.Race;
import org.joda.time.DateTime;
import java.util.Comparator;

/**
 * Created by Barn on 29/12/2017.
 */
public class DateSorter implements Comparator {

    private String order;

    public DateSorter(String order) {
        this.order = order;
    }

    public int compare(Object firstObjToCompare, Object secondObjToCompare) {
        String firstDateString = ((Race) firstObjToCompare).getDate();
        String secondDateString = ((Race) secondObjToCompare).getDate();

        DateTime firstDate;
        DateTime secondDate;

        if (secondDateString == null || firstDateString == null) {
            return 0;
        }

        if (firstDateString.contains("/") | secondDateString.contains("/")) {
            org.joda.time.format.DateTimeFormatter dateTimeFormatter = org.joda.time.format.DateTimeFormat.forPattern("dd/MM/yyyy");
            firstDate = dateTimeFormatter.parseDateTime(firstDateString);
            secondDate = dateTimeFormatter.parseDateTime(secondDateString);
        } else {
            org.joda.time.format.DateTimeFormatter fmt = org.joda.time.format.DateTimeFormat.forPattern("EEE dd MMM");
            firstDate = fmt.parseDateTime(firstDateString);
            secondDate = fmt.parseDateTime(firstDateString);
        }

        if (order.equals("asc")) {
            if (firstDate.isBefore(secondDate)) return -1;
            else if (firstDate.isAfter(secondDate)) return 1;
            else return 0;
        } else {
            if (firstDate.isAfter(secondDate)) return -1;
            else if (firstDate.isBefore(secondDate)) return 1;
            else return 0;
        }

    }
}
