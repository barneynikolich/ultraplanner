package com.example.ultraPlannerBackend.repositories;

import com.example.ultraPlannerBackend.entities.ApexRace;
import com.example.ultraPlannerBackend.entities.Race;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by Barn on 26/12/2017.
 */
@Repository
public interface FinalRaceRepository extends JpaRepository<Race, Long> {
}
