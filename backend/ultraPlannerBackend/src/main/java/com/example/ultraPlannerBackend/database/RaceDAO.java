package com.example.ultraPlannerBackend.database;

import com.example.ultraPlannerBackend.entities.Race;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Barn on 07/12/2017.
 */

//@Transactional
//@Repository
@Component
public class RaceDAO {

    private JdbcTemplate jdbcTemplate;

    public final String RACES_TABLE = "races";

    public RaceDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Race> getAllRaces() {
        List<Race> races = jdbcTemplate.query("select * from "+RACES_TABLE, new RowMapper<Race>() {
            @Override
            public Race mapRow(ResultSet rs, int arg1) throws SQLException {
                Race race = new Race();
                race.setName(rs.getString("name"));
                race.setDate(rs.getString("date"));
                race.setMiles(rs.getString("miles"));
                race.setArea(rs.getString("area"));
                race.setKm(rs.getString("km"));
                race.setEventType(rs.getString("eventType"));
                race.setTerrain(rs.getString("terrain"));
                race.setCost(rs.getString("cost"));
                race.setNotes(rs.getString("notes"));

                return race;
            }
        });
        return races;
    }

    public Race addRace(Race race) {
        System.out.println("Calling Save Race for: " + race);
        String sql = "INSERT INTO "+RACES_TABLE+" (date, name, miles, km, mclimb, area, eventType, terrain, cost, costMile, notes) " +
                "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(sql, race.getDate(), race.getName(), race.getMiles(), race.getKm(), race.getMclimb(), race.getArea(), race.getEventType(),
                race.getTerrain(), race.getCost(), race.getCostMile(), race.getNotes());

        return race;
    }

    public void delete(Race race) {
        System.out.println("Calling Delete Race for: " + race);
        String sql = "DELETE FROM "+RACES_TABLE+" WHERE name = ?";
        jdbcTemplate.update(sql, race.getName());
    }

    public void edit(Race race) {
        System.out.println("Editing ===> " + race.getName());
        String sql = "UPDATE "+RACES_TABLE+" " +
                "SET date = ?, miles = ?, mclimb = ?, area = ?, eventType = ?, terrain = ?, cost = ?, costMile = ?, notes = ? WHERE name = ?";
        jdbcTemplate.update(sql, race.getDate(), race.getMiles(), race.getMclimb(), race.getArea(), race.getEventType(), race.getTerrain(),
                race.getCost(), race.getCostMile(), race.getNotes(),  race.getName());
    }

    public List<Race> filterByTerrain(String terrain) {
        List<Race> filteredRaces = new ArrayList<>();
        System.out.println("Filtering by " + terrain);
        this.getAllRaces().forEach((Race race) -> {
            if (race.getTerrain() != null && race.getTerrain().toLowerCase().contains(terrain.toLowerCase())) {
                System.out.println("Found match on: " + race.getName());
                filteredRaces.add(race);
            }
        });

        return filteredRaces;
    }


    /** Code to create a test DB for races ***
     * CREATE TABLE `ultramarathonDB`.`races` (
     `date` VARCHAR(45) NOT NULL,
     `name` VARCHAR(45) NULL,
     `miles` VARCHAR(45) NULL,
     `km` VARCHAR(45) NULL,
     `mclimb` VARCHAR(45) NULL,
     `mkm` VARCHAR(45) NULL,
     `area` VARCHAR(45) NULL,
     `eventType` VARCHAR(45) NULL,
     `terrain` VARCHAR(45) NULL,
     `cost` VARCHAR(45) NULL,
     `costMile` VARCHAR(45) NULL,
     `notes` VARCHAR(45) NULL);
     */

}
