package com.example.ultraPlannerBackend.controllers.domain;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.persistence.*;

/**
 * Created by Barn on 14/08/2018.
 */
@Entity
@Table(name = "ContactRequests")
public class ContactRequest {

    @Id
    @Column(name = "id")
    @GeneratedValue
    private long id;

    private String firstName;

    private String lastName;

    private String email;

    private String message;

    private String date;

    @Override
    public String toString() {

        return ReflectionToStringBuilder.toString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

    @Override
    public boolean equals(Object request) {
        ContactRequest contactRequest = (ContactRequest) request;
        return contactRequest.getFullName().equals(this.getFullName());
    }

    public String getFullName()
    {
        return this.getFirstName() + this.getLastName();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
