package com.example.ultraPlannerBackend.services;

import com.example.ultraPlannerBackend.controllers.domain.ContactRequest;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.apache.commons.codec.binary.Base64;

@Service
public class MailGunService
{

    private final static String MAILGUN_API = "https://api.mailgun.net/v3/sandbox7d890ddeeb5e4dd2a56360e8e55e76ac.mailgun.org/messages";

    private final static String DESTINATION_EMAIL = "barney.nikolich@gmail.com";

    private final static String API_KEY = "api:bfa35269d8e40ffed6dfae47684003cd-6b60e603-ff15c549";

    public void sendEmail(ContactRequest contactRequest) {
    RestTemplate template = new RestTemplate();
    MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
    map.add("from", "Ultra Planner <postmaster@sandbox7d890ddeeb5e4dd2a56360e8e55e76ac.mailgun.org>");
    map.add("to", "Barney Nikolich <" + DESTINATION_EMAIL + ">");
    map.add("subject", "Ultra Planner enquiry");
    map.add("text", constructMessage(contactRequest));
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
    //Basic Authentication
    String authorisation = API_KEY;
    byte[] encodedAuthorisation = Base64.encodeBase64(authorisation.getBytes());
    headers.add("Authorization", "Basic " + new String(encodedAuthorisation));
    ResponseEntity entity = template.exchange(MAILGUN_API, HttpMethod.POST, new HttpEntity<>(map, headers), String.class);
    }

    private String constructMessage(ContactRequest contactRequest)
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("From: " + contactRequest.getFullName());
        stringBuilder.append("\n");
        stringBuilder.append("Date: " + contactRequest.getDate());
        stringBuilder.append("\n");
        stringBuilder.append("Email: " + contactRequest.getEmail());
        stringBuilder.append("\n");
        stringBuilder.append("Message:\n\n" + contactRequest.getMessage());
        return stringBuilder.toString();
    }
}
