package com.example.ultraPlannerBackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UltraPlannerBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(UltraPlannerBackendApplication.class, args);
    }


}

