
package com.example.ultraPlannerBackend.controllers;

import java.sql.SQLException;

import javax.validation.ValidationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class ServiceExceptionHandler extends ResponseEntityExceptionHandler
{

	private static final Logger LOGGER = LoggerFactory.getLogger(ServiceExceptionHandler.class);

	static final String DATA_BASE_EXCEPTION_TYPE = "SQLException";
	static final String DATA_BASE_EXCEPTION_ERROR_MESSAGE = "Data Base Error";
	static final String VALIDATION_EXCEPTION_TYPE = "ValidationException";
	static final String VALIDATION_EXCEPTION_ERROR_MESSAGE = "Invalid provided data";
	static final String VITIC_WARN_EXCEPTION_TYPE = "ViticWarnException";
	static final String VITIC_HTTP_EXCEPTION_TYPE = "ViticHttpException";
	static final String EXCEPTION_TYPE = "GeneralException";
	static final String EXCEPTION_ERROR_MESSAGE = "Unknown Error";
	public static final String UNAUTHORIZED_MESSAGE = "Unauthorized access, user is not logged in";

	@ExceptionHandler({ SQLException.class, DataAccessException.class })
	protected ResponseEntity<Object> handleGeneralSQLException(Exception e, WebRequest request)
	{
		return generateErrorResponse(request, e, DATA_BASE_EXCEPTION_ERROR_MESSAGE, DATA_BASE_EXCEPTION_TYPE, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler({ ValidationException.class })
	protected ResponseEntity<Object> handleGeneralValidationException(ValidationException e, WebRequest request)
	{
		return generateInfoResponse(request, e, VALIDATION_EXCEPTION_ERROR_MESSAGE, VALIDATION_EXCEPTION_TYPE, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler({ Exception.class })
	protected ResponseEntity<Object> handleGeneralException(Exception e, WebRequest request)
	{
		return generateErrorResponse(request, e, EXCEPTION_ERROR_MESSAGE, EXCEPTION_TYPE, HttpStatus.INTERNAL_SERVER_ERROR);
	}

//	@ExceptionHandler({ ViticWarnException.class })
//	protected ResponseEntity<Object> handleViticWarningException(ViticWarnException e, WebRequest request)
//	{
//		return generateWarningResponse(request, e, VITIC_WARN_EXCEPTION_TYPE, HttpStatus.INTERNAL_SERVER_ERROR);
//	}
//
//	@ExceptionHandler({ ViticHttpException.class })
//	protected ResponseEntity<Object> handleViticHttpException(ViticHttpException e, WebRequest request)
//	{
//		return generateViticHttpException(request, e, VITIC_HTTP_EXCEPTION_TYPE, e.getHttpStatus());
//	}
//
	private ResponseEntity<Object> generateErrorResponse(WebRequest request, Exception e, String errorMessage, String exceptionType, HttpStatus httpStatus)
	{
		LOGGER.error(String.format("[%s]: Handling Error: ", e));
		return handleExceptionInternal(request, e, errorMessage, exceptionType, httpStatus);
	}

//	private ResponseEntity<Object> generateWarningResponse(WebRequest request, ViticWarnException e, String exceptionType, HttpStatus httpStatus)
//	{
//		LOGGER.warn(String.format("[%s]: Handling Warning: ", LogEventKey.EXCEPTION_HANDLER), e);
//		return handleExceptionInternal(request, e, e.getDisplayMessage(), exceptionType, httpStatus);
//	}

//	private ResponseEntity<Object> generateViticHttpException(WebRequest request, ViticHttpException e, String exceptionType, HttpStatus httpStatus)
//	{
//		LOGGER.warn(String.format("[%s]: Handling %s: ", LogEventKey.EXCEPTION_HANDLER, httpStatus.value()), e);
//		return handleExceptionInternal(request, e, e.getDisplayMessage(), exceptionType, httpStatus);
//	}

//	 Clearly this method is called above
//	@SuppressWarnings("squid:UnusedPrivateMethod")
	private ResponseEntity<Object> generateInfoResponse(WebRequest request, Exception e, String errorMessage, String exceptionType, HttpStatus httpStatus)
	{
		LOGGER.info("[{}]: Handling Info {}", e.getMessage());
		return handleExceptionInternal(request, e, errorMessage, exceptionType, httpStatus);
	}

	private ResponseEntity<Object> handleExceptionInternal(WebRequest request, Exception e, String errorMessage, String exceptionType, HttpStatus httpStatus)
	{
		String jsonResponse = new ServerFailureResponse().generate(request, errorMessage, exceptionType, httpStatus);
		return handleExceptionInternal(e, jsonResponse, getHeader(), httpStatus, request);
	}

	private HttpHeaders getHeader()
	{
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return headers;
	}
}


