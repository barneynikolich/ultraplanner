package com.example.ultraPlannerBackend.testData;

import com.example.ultraPlannerBackend.entities.Race;
import com.example.ultraPlannerBackend.utils.DateSorter;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Barn on 29/12/2017.
 */
public class DateTests {

    @Test
    public void getJodaDateFromString() {
        List<Race> races = testRacesList();
//        races.sort((d1, d2) -> getDateFromString(d1.getDate()).compareTo(getDateFromString(d2.getDate())));

        DateTimeFormatter dateformat = DateTimeFormat.forPattern("dd/MM/yyyy");

        for (Race race : races) {
            DateTime raceDate = dateformat.parseDateTime(race.getDate());
            int month = raceDate.getMonthOfYear();

            if (month == DateTime.now().getMonthOfYear()) {
                System.out.println("ENTERED " + race);
            }
        }
    }

    private Date getDateFromString(String date) {
        SimpleDateFormat parseFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date newDate;
        try {
            newDate = parseFormat.parse(date);
        } catch (ParseException e) {
            newDate = null;
            e.printStackTrace();
        }
        return newDate;
    }

    private List<Race> testRacesList() {
        final List<Race> races = new ArrayList<>();

        Race race1 = new Race();
        race1.setName("The Fellsman");
        race1.setDate("3/12/2018");

        Race race2 = new Race();
        race2.setName("Hardmoor 60");
        race2.setDate("01/12/2017");
        race2.setWebsite("www.thisisatest.com");

        Race race3 = new Race();
        race3.setName("Cheese");
        race3.setDate("02/12/2017");

        Race race4 = new Race();
        race4.setName("Rat race");
        race4.setDate("06/12/2017");

        Race race5 = new Race();
        race5.setName("Three Peaks");
        race5.setDate("20/2/2017");

        Race race6 = new Race();
        race6.setName("Three Peaks");
        race6.setDate("20/2/2017");

        races.add(race1);
        races.add(race2);
        races.add(race3);
        races.add(race4);
        races.add(race5);
        races.add(race6);

        return races;
    }

}
