///**
// * Copyright © 2017 Lhasa Limited
// * File created: 23 Feb 2017 by Mohammed Sultan
// * Creator : Mohammed Sultan
// * Version : $Id$
// */
//package com.example.ultraPlannerBackend.controllers;
//
//import static org.hamcrest.Matchers.is;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.runners.MockitoJUnitRunner;
//import org.springframework.http.MediaType;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//
///**
// * @author Mohammed Sultan
// * @since 23 Feb 2017
// */
//@RunWith(MockitoJUnitRunner.class)
//public class ServiceExceptionHandlerTest
//{
//	private static final String EXCEPTION_ELEM_NAME = "$.exception";
//	private static final String MESSAGE_ELEM_NAME = "$.message";
//	private static final String VALIDATION_EXCEPTION_URL = "/throwValidationException";
//	private static final String VALIDATION_EXCEPTION_DESC = "ValidationException";
//	private static final String SQL_EXCEPTION_URL = "/throwSQLException";
//	private static final String DATA_ACCESS_EXCEPTION_URL = "/throwDataAccessException";
//	private static final String SQL_EXCEPTION_DESC = "SQLException";
//	private static final String GENERAL_EXCEPTION_URL = "/throwGeneralException";
//	private static final String GENERAL_EXCEPTION_DESC = "GeneralException";
//	private static final String VITIC_WARN_EXCEPTION_URL = "/throwViticWarnException";
//	private static final String VITIC_HTTP_STATUS_EXCEPTION_URL = "/throwViticHttpException";
//	private static final String VITIC_HTTP_STATUS_EXCEPTION_WITH_DISPLAY_MESSAGE_URL = "/throwViticHttpExceptionwithDisplayMessage";
//	private static final String VITIC_WARN_EXCEPTION_WITH_DISPLAY_MESSAGE_URL = "/throwViticWarnExceptionWithDisplayMessage";
//	private static final String VITIC_WARN_EXCEPTION_DESC = "ViticWarnException";
//	private MockMvc mockMvc;
//
//	@Before
//	public void init()
//	{
//		this.mockMvc = MockMvcBuilders.standaloneSetup(new TestController())
//				.setControllerAdvice(new ServiceExceptionHandler())
//				.alwaysExpect(content().contentType(MediaType.APPLICATION_JSON))
//				.build();
//	}
//
//	@Test
//	public void shouldReturnBadWhenWhenInvalidUrl() throws Exception
//	{
//		this.mockMvc.perform(get(VALIDATION_EXCEPTION_URL))
//				.andExpect(status().isBadRequest())
//				.andExpect(jsonPath(EXCEPTION_ELEM_NAME, is(VALIDATION_EXCEPTION_DESC)));
//	}
//
//	@Test
//	public void shouldReturnInternalServerErrorWhenDbFails() throws Exception
//	{
//		this.mockMvc.perform(get(SQL_EXCEPTION_URL))
//				.andExpect(status().isInternalServerError())
//				.andExpect(jsonPath(EXCEPTION_ELEM_NAME, is(SQL_EXCEPTION_DESC)));
//		this.mockMvc.perform(get(DATA_ACCESS_EXCEPTION_URL))
//				.andExpect(status().isInternalServerError())
//				.andExpect(jsonPath(EXCEPTION_ELEM_NAME, is(SQL_EXCEPTION_DESC)));
//
//	}
//
//	@Test
//	public void shouldReturnInternalServerErrorWhenGeneralException() throws Exception
//	{
//		this.mockMvc.perform(get(GENERAL_EXCEPTION_URL))
//				.andExpect(status().isInternalServerError())
//				.andExpect(jsonPath(EXCEPTION_ELEM_NAME, is(GENERAL_EXCEPTION_DESC)));
//
//	}
//
//	@Test
//	public void shouldReturnInternalServerErrorWhenViticWarnException() throws Exception
//	{
//		this.mockMvc.perform(get(VITIC_WARN_EXCEPTION_URL))
//				.andExpect(status().isInternalServerError())
//				.andExpect(jsonPath(EXCEPTION_ELEM_NAME, is(VITIC_WARN_EXCEPTION_DESC)));
//
//	}
//
//	@Test
//	public void shouldReturnDisplayMessageWhenViticWarnExceptionHasDisplayMessage() throws Exception
//	{
//		this.mockMvc.perform(get(VITIC_WARN_EXCEPTION_WITH_DISPLAY_MESSAGE_URL))
//				.andExpect(jsonPath(MESSAGE_ELEM_NAME, is("display message")));
//
//	}
//
//	@Test
//	public void shouldReturnMessageWhenViticWarnExceptionHasNoDisplayMessage() throws Exception
//	{
//		this.mockMvc.perform(get(VITIC_WARN_EXCEPTION_URL))
//				.andExpect(jsonPath(MESSAGE_ELEM_NAME, is("log message")));
//
//	}
//
//	@Test
//	public void shouldReturnMessageWhenViticHttpStatusException() throws Exception
//	{
//		this.mockMvc.perform(get(VITIC_HTTP_STATUS_EXCEPTION_URL))
//				.andExpect(jsonPath(MESSAGE_ELEM_NAME, is("log message")))
//				.andExpect(status().is4xxClientError());
//
//	}
//
//	@Test
//	public void shouldReturnMessageWhenViticHttpStatusExceptionHasDisplayMessage() throws Exception
//	{
//		this.mockMvc.perform(get(VITIC_HTTP_STATUS_EXCEPTION_WITH_DISPLAY_MESSAGE_URL))
//				.andExpect(jsonPath(MESSAGE_ELEM_NAME, is("display message")))
//				.andExpect(status().is4xxClientError());;
//	}
//
//}
///* ---------------------------------------------------------------------*
// * This software is the confidential and proprietary
// * information of Lhasa Limited
// * Granary Wharf House, 2 Canal Wharf, Leeds, LS11 5PS
// * ---
// * No part of this confidential information shall be disclosed
// * and it shall be used only in accordance with the terms of a
// * written license agreement entered into by holder of the information
// * with LHASA Ltd.
// * --------------------------------------------------------------------- */
