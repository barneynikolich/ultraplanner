package com.example.ultraPlannerBackend.services;

import com.example.ultraPlannerBackend.database.RaceDAO;
import com.example.ultraPlannerBackend.entities.Race;
import com.example.ultraPlannerBackend.entities.RaceBean;
import com.example.ultraPlannerBackend.repositories.RaceRepository;
import org.hamcrest.core.IsEqual;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RaceDataServiceTest {

    @InjectMocks
    RaceDataServiceImpl subject;

    @Mock
    RaceDAO mockRaceDao;

    @Mock
    RaceRepository mockRaceRepository;

    @Test
    public void testSaveRace() {
        final Race race = new Race();
        race.setName("The Fellsman");

        final Race raceResponse = new Race();
        raceResponse.setId(100);
        raceResponse.setName("The Fellsman");

        when(mockRaceRepository.save(race)).thenReturn(raceResponse);

        Race response = subject.save(race);

        assertThat(response.getName(), equalTo("The Fellsman"));
    }



//    @Test
//    public void findAllWithNoParametersShouldReturnAllRaces() {
//
//        when(mockRaceRepository.findAll()).thenReturn(testRacesList());
//
//
//        List<Race> response = subject.findAll(Optional.empty(), Optional.empty(), Optional.empty());
//        assertThat(response.size(), equalTo(3));
//    }

    private List<Race> testRacesList() {
        final List<Race> races = new ArrayList<>();

        Race race1 = new Race();
        race1.setName("The Fellsman");

        Race race2 = new Race();
        race1.setName("Hardmoor 60");

        Race race3 = new Race();
        race1.setName("some race");

        races.add(race1);
        races.add(race2);
        races.add(race3);

        return races;
    }

}
