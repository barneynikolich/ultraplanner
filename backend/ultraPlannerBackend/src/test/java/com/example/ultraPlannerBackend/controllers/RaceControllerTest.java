package com.example.ultraPlannerBackend.controllers;
import com.example.ultraPlannerBackend.entities.Race;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import com.example.ultraPlannerBackend.services.RaceDataService;
import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.net.URI;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.any;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.servlet.View;

@RunWith(MockitoJUnitRunner.class)
public class RaceControllerTest {

    private static final String RACES_ENDPOINT = "/api/v1/races";
    private static final String UPCOMING_ENDPOINT = RACES_ENDPOINT + "/upcoming";

    @InjectMocks
    RaceController classUnderTest;

    @Mock
    private RaceDataService mockRaceDataService;

    private MockMvc mockMvc;

    @Before
    public void init()
    {
        this.mockMvc = MockMvcBuilders.standaloneSetup(this.classUnderTest)
                .setControllerAdvice(new ServiceExceptionHandler())
                .alwaysDo(MockMvcResultHandlers.print())
                .build();
    }

    @Test
    public void getUpcomingRaces_shouldReturnAListOfRacesJsonResponse() throws Exception
    {
        when(mockRaceDataService.getUpcomingRaces()).thenReturn(aRacesList());

        this.mockMvc.perform(get(new URI(UPCOMING_ENDPOINT)).contentType(MediaType.APPLICATION_JSON_UTF8).accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].name", is("Fellsman")))
                .andExpect(jsonPath("$[1].name", is("Hardmoor 60")))
                .andExpect(jsonPath("$[2].name", is("some race")));
    }

    @Test
    public void findRaceById_shouldReturnASingleRace() throws Exception
    {
        when(mockRaceDataService.findRace(3L)).thenReturn(aRace("Hedgehope", "Northumberland", "22", "Fell"));

        this.mockMvc.perform(get(new URI(RACES_ENDPOINT + "/3")).contentType(MediaType.APPLICATION_JSON_UTF8).accept(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().is2xxSuccessful())
                .andExpect(jsonPath("$.name", is("Hedgehope")))
                .andExpect(jsonPath("$.area", is("Northumberland")))
                .andExpect(jsonPath("$.miles", is("22")));
    }

    @Test
    public void saveRace_shouldCallRaceDataServiceWithRace() throws Exception
    {
        Race race = aRace("race", "area", "miles", "terrain");
        when(mockRaceDataService.save(Matchers.any())).thenReturn(race);
        this.mockMvc.perform(post(new URI(RACES_ENDPOINT))
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(buildValidRaceResponse(race)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name", is("race")));
    }

//    @Test
//    public void shouldReturnInternalServerErrorWhenDbFails() throws Exception
//    {
//        this.mockMvc.perform(get(SQL_EXCEPTION_URL))
//                .andExpect(status().isInternalServerError())
//                .andExpect(jsonPath(EXCEPTION_ELEM_NAME, is(SQL_EXCEPTION_DESC)));
//        this.mockMvc.perform(get(DATA_ACCESS_EXCEPTION_URL))
//                .andExpect(status().isInternalServerError())
//                .andExpect(jsonPath(EXCEPTION_ELEM_NAME, is(SQL_EXCEPTION_DESC)));
//
//    }

    @Test
    public void testSavRaceReturnsSavedRace() {
        Race raceRequest = new Race();
        Race raceResponse = new Race();
        raceResponse.setName("The Fellsman");


        when(mockRaceDataService.save(raceRequest)).thenReturn(raceResponse);
        assertThat(classUnderTest.saveRace(raceRequest).getName(), equalTo( "The Fellsman"));
    }

    private List<Race> aRacesList() {
        Race race1 = aRace("Fellsman", "Yorkshire Dales", "62", "Fell");
        Race race2 = aRace("Hardmoor 60", "North York Moors", "60", "Coastal");
        Race race3 = aRace("some race", "Some place", "44", "somewhere");

        return Arrays.asList(race1, race2, race3);
    }

    private Race aRace(String name, String area, String miles, String terrain)
    {
        Race race = new Race();
        race.setName(name);
        race.setArea(area);
        race.setMiles(miles);
        race.setTerrain(terrain);
        return race;
    }

    private String buildValidRaceResponse(Race race)
    {
        Gson gson = new Gson();
        return gson.toJson(race);

    }
}

