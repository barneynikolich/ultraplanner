package com.example.ultraPlannerBackend.services;

import com.example.ultraPlannerBackend.controllers.domain.ContactRequest;
import com.example.ultraPlannerBackend.repositories.ContactRequestRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class ContactRequestServiceTest
{

    @Mock
    MailGunService mockMailGunService;

    @Mock
    ContactRequestRepository mockContactRequestRepository;

    @InjectMocks
    ContactRequestService serviceUnderTest;


    @Test
    public void shouldSaveContactRequest()
    {
        this.serviceUnderTest.saveRequest(aContactRequest());
        Mockito.verify(mockContactRequestRepository, times(1)).save(aContactRequest());
    }

    @Test
    public void shouldCallMailService()
    {
        this.serviceUnderTest.saveRequest(aContactRequest());
        Mockito.verify(mockMailGunService, times(1)).sendEmail(aContactRequest());

    }

    private ContactRequest aContactRequest()
    {
        ContactRequest contactRequest = new ContactRequest();
        contactRequest.setDate("12/10/1994");
        contactRequest.setFirstName("firstname");
        contactRequest.setLastName("secondname");
        contactRequest.setEmail("test@email.com");
        contactRequest.setMessage("This is a test message");

        return contactRequest;
    }

}
