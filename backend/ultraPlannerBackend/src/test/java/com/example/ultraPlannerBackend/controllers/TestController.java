///**
// * Copyright © 2017 Lhasa Limited
// * File created: 23 Feb 2017 by Mohammed Sultan
// * Creator : Mohammed Sultan
// * Version : $Id$
// */
//package com.example.ultraPlannerBackend.controllers;
//
//import java.sql.SQLException;
//
//import javax.validation.Valid;
//import javax.validation.ValidationException;
//import javax.validation.constraints.NotNull;
//
//import org.lhasalimited.vitic.backend.web.ViticHttpException;
//import org.lhasalimited.vitic.backend.web.ViticWarnException;
//import org.springframework.dao.PermissionDeniedDataAccessException;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.MediaType;
//import org.springframework.web.bind.annotation.RequestBody;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RestController;
//
///**
// * @author Mohammed Sultan
// * @since 23 Feb 2017
// */
//@RestController
//public class TestController
//{
//
//	@RequestMapping(value = "/throwValidationException", method = RequestMethod.GET)
//	public void throwValidationException()
//	{
//		throw new ValidationException("test validation exception");
//	}
//
//	@RequestMapping(value = "/throwSQLException", method = RequestMethod.GET)
//	public void throwthrowSQLException() throws SQLException
//	{
//		throw new SQLException();
//	}
//
//	@RequestMapping(value = "/throwDataAccessException", method = RequestMethod.GET)
//	public void throwDataAccessException()
//	{
//		throw new PermissionDeniedDataAccessException("test Data Access Exception", null);
//	}
//
//	@RequestMapping(value = "/throwViticWarnException", method = RequestMethod.GET)
//	public void throwViticWarnException()
//	{
//		throw new ViticWarnException("log message", new Object[] {});
//	}
//
//	@RequestMapping(value = "/throwViticWarnExceptionWithDisplayMessage", method = RequestMethod.GET)
//	public void throwViticWarnExceptionWithDisplayMessage()
//	{
//		throw new ViticWarnException("log message", "display message", null, null);
//	}
//
//	@RequestMapping(value = "/throwViticHttpException", method = RequestMethod.GET)
//	public void throwViticHttpException()
//	{
//		throw new ViticHttpException("log message", HttpStatus.UNAUTHORIZED);
//	}
//
//	@RequestMapping(value = "/throwViticHttpExceptionwithDisplayMessage", method = RequestMethod.GET)
//	public void throwViticHttpExceptionWithDisplayMessage()
//	{
//		throw new ViticHttpException("log message", HttpStatus.UNAUTHORIZED, "display message", null, null);
//	}
//
//	@RequestMapping(value = "/throwGeneralException", method = RequestMethod.GET)
//	public void throwArthermaticException()
//	{
//		throw new ArithmeticException();
//	}
//
//	@RequestMapping(value = "/throwMethodArgumentNotValidException", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	public void methodArgumentNotValidException(@RequestBody @Valid QueryRequest request)
//	{
//	}
//
//	private static class QueryRequest
//	{
//
//		@NotNull
//		private String matchType;
//
//	}
//
//}
//
///* ---------------------------------------------------------------------*
// * This software is the confidential and proprietary
// * information of Lhasa Limited
// * Granary Wharf House, 2 Canal Wharf, Leeds, LS11 5PS
// * ---
// * No part of this confidential information shall be disclosed
// * and it shall be used only in accordance with the terms of a
// * written license agreement entered into by holder of the information
// * with LHASA Ltd.
// * --------------------------------------------------------------------- */
